# ROGUE SNAKE

A small Unity game made by Bouchier Tristan & Prangère Romain for the sake of
the unity community discord game jam number 17

![Demo Scene](Docs/Images/CaptureDuJeu.png)

## Changelog

 - 03-05-2021
   - Creation of the gitlab repository
 - 03-06-2021
   - First Version
   
## To play the game

To be able to simply play the game without bothering yourself you can just:

 1. Download the last build of the game [from itch.io](https://lavande-potions.itch.io/rogue-snake).
 2. Unzipp the files downloaded from the drive on your computer.
 3. Run the executable

![Unity Configuration](Docs/Images/Image2.jpg)

## Configuration for Unity

To be able to use this project you'll have to set it up first:

 1. Download the original full sources and unversionned files [from our drive](https://drive.google.com/file/d/1fvbwU8T8YURa2shjdbh5B50zSiHgvV8h/view?usp=sharing).
 2. Initiate this git repository somewhere on your computer.
 3. Unzipp the files downloaded from the drive (1) inside the git repository (2) initiated.
 4. Pull master inside the repository to erase the folder with the lasts modifications.
 5. Start unity, locate the folder and it should run.
 
## Testing locally

To test locally you can create a standalone build:

 1. In the menu bar click on `File > Build Settings...`
 2. Click the `Build` button, choose a destination folder and name your executable.
 3. In the Explorer window that popped up right-click on the `.exe` and select `Create shortcut`
 4. Click `Ok`
 5. Run the executable by double-clicking the shortcut

Or you can test in Unity by just clicking on "Play"


## TODO
 
- Do the project
- ???
- Win the jam
https://itch.io/jam/udc-jam-17/rate/957557
