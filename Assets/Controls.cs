// GENERATED AUTOMATICALLY FROM 'Assets/Controls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace roguelikesnake
{
    public class @Controls : IInputActionCollection, IDisposable
    {
        public InputActionAsset asset { get; }
        public @Controls()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""Controls"",
    ""maps"": [
        {
            ""name"": ""PlayerGame"",
            ""id"": ""50d1aa8a-1799-443a-a8f9-5eda4076a8be"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""Value"",
                    ""id"": ""5f2b69d5-adeb-4588-909b-a18f0f1ebeaa"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Scroll"",
                    ""type"": ""Value"",
                    ""id"": ""720874b4-0320-4313-aacd-4ccea94b3c60"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""CameraMovementsX"",
                    ""type"": ""Value"",
                    ""id"": ""26b42e0c-e732-4a7e-893f-8b7d2b02de1d"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""CameraMovementY"",
                    ""type"": ""Value"",
                    ""id"": ""e1455f8d-6f16-4303-9ff0-3618ffed4892"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""RightClick"",
                    ""type"": ""Button"",
                    ""id"": ""a560c44e-f678-4955-9bc4-33a12d74af06"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Restart"",
                    ""type"": ""Button"",
                    ""id"": ""69494600-ad51-43d8-a101-a7f5f921aeea"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""ZQSD"",
                    ""id"": ""bbea19f0-f526-4077-8185-9fa1bdfb32e4"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""711da546-c5bf-4979-87d5-567e80d017cd"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""e832b995-322d-4d01-bb44-b1c26d30c760"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""9f14d604-37ca-4746-ab58-6fdd4086a1c3"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""c5440f33-b942-4dbb-86cd-bdb5eacd9a02"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""fc0c503e-b627-447f-8514-d91eaa8e9641"",
                    ""path"": ""<Mouse>/scroll"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Scroll"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1af90c77-909c-42f1-a898-f3e0942ac1e3"",
                    ""path"": ""<Mouse>/delta/x"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CameraMovementsX"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ca44bdb9-01fb-4f5e-bf1a-466cf2976046"",
                    ""path"": ""<Mouse>/delta/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CameraMovementY"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""de15d28d-4bc4-43da-bde4-c8f5b6fdd9b1"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RightClick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2b73f47c-efdc-4978-88b8-e112ede11e09"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Restart"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
            // PlayerGame
            m_PlayerGame = asset.FindActionMap("PlayerGame", throwIfNotFound: true);
            m_PlayerGame_Move = m_PlayerGame.FindAction("Move", throwIfNotFound: true);
            m_PlayerGame_Scroll = m_PlayerGame.FindAction("Scroll", throwIfNotFound: true);
            m_PlayerGame_CameraMovementsX = m_PlayerGame.FindAction("CameraMovementsX", throwIfNotFound: true);
            m_PlayerGame_CameraMovementY = m_PlayerGame.FindAction("CameraMovementY", throwIfNotFound: true);
            m_PlayerGame_RightClick = m_PlayerGame.FindAction("RightClick", throwIfNotFound: true);
            m_PlayerGame_Restart = m_PlayerGame.FindAction("Restart", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // PlayerGame
        private readonly InputActionMap m_PlayerGame;
        private IPlayerGameActions m_PlayerGameActionsCallbackInterface;
        private readonly InputAction m_PlayerGame_Move;
        private readonly InputAction m_PlayerGame_Scroll;
        private readonly InputAction m_PlayerGame_CameraMovementsX;
        private readonly InputAction m_PlayerGame_CameraMovementY;
        private readonly InputAction m_PlayerGame_RightClick;
        private readonly InputAction m_PlayerGame_Restart;
        public struct PlayerGameActions
        {
            private @Controls m_Wrapper;
            public PlayerGameActions(@Controls wrapper) { m_Wrapper = wrapper; }
            public InputAction @Move => m_Wrapper.m_PlayerGame_Move;
            public InputAction @Scroll => m_Wrapper.m_PlayerGame_Scroll;
            public InputAction @CameraMovementsX => m_Wrapper.m_PlayerGame_CameraMovementsX;
            public InputAction @CameraMovementY => m_Wrapper.m_PlayerGame_CameraMovementY;
            public InputAction @RightClick => m_Wrapper.m_PlayerGame_RightClick;
            public InputAction @Restart => m_Wrapper.m_PlayerGame_Restart;
            public InputActionMap Get() { return m_Wrapper.m_PlayerGame; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(PlayerGameActions set) { return set.Get(); }
            public void SetCallbacks(IPlayerGameActions instance)
            {
                if (m_Wrapper.m_PlayerGameActionsCallbackInterface != null)
                {
                    @Move.started -= m_Wrapper.m_PlayerGameActionsCallbackInterface.OnMove;
                    @Move.performed -= m_Wrapper.m_PlayerGameActionsCallbackInterface.OnMove;
                    @Move.canceled -= m_Wrapper.m_PlayerGameActionsCallbackInterface.OnMove;
                    @Scroll.started -= m_Wrapper.m_PlayerGameActionsCallbackInterface.OnScroll;
                    @Scroll.performed -= m_Wrapper.m_PlayerGameActionsCallbackInterface.OnScroll;
                    @Scroll.canceled -= m_Wrapper.m_PlayerGameActionsCallbackInterface.OnScroll;
                    @CameraMovementsX.started -= m_Wrapper.m_PlayerGameActionsCallbackInterface.OnCameraMovementsX;
                    @CameraMovementsX.performed -= m_Wrapper.m_PlayerGameActionsCallbackInterface.OnCameraMovementsX;
                    @CameraMovementsX.canceled -= m_Wrapper.m_PlayerGameActionsCallbackInterface.OnCameraMovementsX;
                    @CameraMovementY.started -= m_Wrapper.m_PlayerGameActionsCallbackInterface.OnCameraMovementY;
                    @CameraMovementY.performed -= m_Wrapper.m_PlayerGameActionsCallbackInterface.OnCameraMovementY;
                    @CameraMovementY.canceled -= m_Wrapper.m_PlayerGameActionsCallbackInterface.OnCameraMovementY;
                    @RightClick.started -= m_Wrapper.m_PlayerGameActionsCallbackInterface.OnRightClick;
                    @RightClick.performed -= m_Wrapper.m_PlayerGameActionsCallbackInterface.OnRightClick;
                    @RightClick.canceled -= m_Wrapper.m_PlayerGameActionsCallbackInterface.OnRightClick;
                    @Restart.started -= m_Wrapper.m_PlayerGameActionsCallbackInterface.OnRestart;
                    @Restart.performed -= m_Wrapper.m_PlayerGameActionsCallbackInterface.OnRestart;
                    @Restart.canceled -= m_Wrapper.m_PlayerGameActionsCallbackInterface.OnRestart;
                }
                m_Wrapper.m_PlayerGameActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @Move.started += instance.OnMove;
                    @Move.performed += instance.OnMove;
                    @Move.canceled += instance.OnMove;
                    @Scroll.started += instance.OnScroll;
                    @Scroll.performed += instance.OnScroll;
                    @Scroll.canceled += instance.OnScroll;
                    @CameraMovementsX.started += instance.OnCameraMovementsX;
                    @CameraMovementsX.performed += instance.OnCameraMovementsX;
                    @CameraMovementsX.canceled += instance.OnCameraMovementsX;
                    @CameraMovementY.started += instance.OnCameraMovementY;
                    @CameraMovementY.performed += instance.OnCameraMovementY;
                    @CameraMovementY.canceled += instance.OnCameraMovementY;
                    @RightClick.started += instance.OnRightClick;
                    @RightClick.performed += instance.OnRightClick;
                    @RightClick.canceled += instance.OnRightClick;
                    @Restart.started += instance.OnRestart;
                    @Restart.performed += instance.OnRestart;
                    @Restart.canceled += instance.OnRestart;
                }
            }
        }
        public PlayerGameActions @PlayerGame => new PlayerGameActions(this);
        public interface IPlayerGameActions
        {
            void OnMove(InputAction.CallbackContext context);
            void OnScroll(InputAction.CallbackContext context);
            void OnCameraMovementsX(InputAction.CallbackContext context);
            void OnCameraMovementY(InputAction.CallbackContext context);
            void OnRightClick(InputAction.CallbackContext context);
            void OnRestart(InputAction.CallbackContext context);
        }
    }
}
