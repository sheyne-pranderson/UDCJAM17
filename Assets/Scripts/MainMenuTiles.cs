﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace roguelikesnake
{
    public class MainMenuTiles : MonoBehaviour
    {
        private float sin = 3f;
        private float sinHeigh = 12f;
        private float baseHeigh;
        private float startSin;

        // Start is called before the first frame update
        void Start()
        {
            startSin = Random.Range(-0.5f, 0.5f);
            sin += startSin;
        }

        void Update()
        {
            transform.position = new Vector3(transform.position.x, baseHeigh + Mathf.Sin(Time.time * sin) / sinHeigh, transform.position.z);
        }
    }
}