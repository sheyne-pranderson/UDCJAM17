﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace roguelikesnake
{
    public class UIController : MonoBehaviour
    {
		private GameManager gm;
        public GameObject thisCanvas;

        private int actualHp;
        private int maxHp;

        [Header("HP")]
        public Slider barHP;
        public Text txtHP;

        private int actualScore;
        private int minScore;
        private int maxScore;

        [Header("Data")]
        public Slider barDificultyLevel;
        public Text txtDificultyLevel;

        private int nbrBrokenTile;
        public Text txtBrokenTile;

        private int actualGold = 0;
        public Text txtGold;

        private int actualDifficultyLevel = 1;
        public Text txtDificulty;

        [Header("DebugBtn")]
        public GameObject btnRestart;
        public GameObject btnNextLevel;

        // Start is called before the first frame update
        void Start()
        {
            gm = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
            GameEvents.current.OnUpgradingUI += UpgradeALL;
            GameEvents.current.OnPlayerWin += OtherUIOpened;
            GameEvents.current.OnPlayerCollideWallOrVoid += OtherUIOpened;
            if (Application.isPlaying && Application.isEditor)
            {
                btnRestart.SetActive(true);
                btnNextLevel.SetActive(true);
            }
            UpgradeALL();
        }

        // Update is called once per frame
        void Update()
        {
            UpdateDataFromGM();
            UIUpdate();
        }

        void UpgradeALL()
		{
            thisCanvas.SetActive(true);
            UpgradeDataFromGM();
            UIUpgrade();
        }

        void UpdateDataFromGM()
        {
            actualHp = gm.actualHp;
            actualScore = gm.actualLevelScore;
            actualDifficultyLevel = gm.actualDifficultyLevel;
            nbrBrokenTile = gm.nbrBrokenTile;
            actualGold = gm.actualGold;
        }

        void UpgradeDataFromGM()
        {
            UpdateDataFromGM();
            maxHp = gm.maxHp;
            minScore = gm.minScore;
            maxScore = gm.maxScore;
        }

        public void UIUpdate()
        {
            HpUpdate();
            DlUpdate();
            BtUpdate();
        }

        public void UIUpgrade()
        {
            HpUpgrade();
            DlUpgrade();
            BtUpdate();
        }

        public void HpUpdate()
        {
            txtHP.text = actualHp + "/" + maxHp;
            txtGold.text = actualGold.ToString();
            txtDificulty.text = actualDifficultyLevel.ToString();
            barHP.value = actualHp;
        }

        public void HpUpgrade()
        {
            barHP.maxValue = maxHp;
            HpUpdate();
        }

        public void DlUpdate()
        {
            txtDificultyLevel.text = actualScore + "/" + maxScore;
            barDificultyLevel.value = actualScore;
        }

        public void DlUpgrade()
        {
            barDificultyLevel.minValue = minScore;
            barDificultyLevel.maxValue = maxScore;
            DlUpdate();
        }

        public void BtUpdate()
        {
            txtBrokenTile.text = nbrBrokenTile.ToString();
        }

        public void OtherUIOpened()
		{
            thisCanvas.SetActive(false);
        }
    }
}
