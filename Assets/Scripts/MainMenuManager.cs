﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using roguelikesnake;

public class MainMenuManager : MonoBehaviour
{
    [Header("Sound Volume")]
    public float soundMult;
    public Slider soundSlider;
    public float musicMult;
    public Slider musicSlider;

    [Header("Style")]
    public Color txtCol;
    public Color playBtnCol;
    public Color secondBtnCol;
    public Color quitBtnCol;

    [Header("Btn")]
    public Image playBtn;
    public Text playBtnTxt;

    public Image howToBtn;
    public Text howToBtnTxt;

    public Image settingBtn;
    public Text settingBtnTxt;

    public Image quitBtn;
    public Text quitBtnTxt;

    [Header("Canvas")]
    public Canvas mainMenu;
    public GameObject mmOBJ;

    public Canvas howToMenu;
    public GameObject htOBJ;

    public Canvas settingMenu;
    public GameObject seOBJ;

    [Header("How to pages")]
    private int actualPageHowTo = 1;
    public GameObject p1;
    public GameObject p2;
    public GameObject p3;
    public GameObject p4;
    public GameObject p5;
    public GameObject p6;
    public GameObject p7;
    public GameObject btnNEXT;
    public GameObject btnPREV;

    public UiFade FadeIn;


    // Start is called before the first frame update
    void Start()
    {
        playBtn.color = playBtnCol;
        howToBtn.color = secondBtnCol;
        settingBtn.color = secondBtnCol;
        quitBtn.color = quitBtnCol;

        playBtnTxt.color = txtCol;
        howToBtnTxt.color = txtCol;
        settingBtnTxt.color = txtCol;
        quitBtnTxt.color = txtCol;

		if (PlayerPrefs.GetFloat("soundMult", -1) != -1 && PlayerPrefs.GetFloat("musicMult", -1) != -1)
		{
            soundSlider.value = PlayerPrefs.GetFloat("soundMult");
            musicSlider.value = PlayerPrefs.GetFloat("musicMult");
        }
		else
		{
            soundSlider.value = 0.5f;
            musicSlider.value = 0.5f;
            PlayerPrefs.SetFloat("soundMult", 0.5f);
            PlayerPrefs.SetFloat("musicMult", 0.5f);
        }
        SoundManager.current.musicMenu();

        ChangeCanvas("mainMenu");

    }

    // Update is called once per frame
    void Update()
    {
		if (settingMenu.gameObject.activeInHierarchy)
		{
            soundMult = soundSlider.value;
            musicMult = musicSlider.value;
        }
    }

    public void BackToMainMenu()
	{
        ChangeCanvas("mainMenu");
        actualPageHowTo = 1;
        PlayerPrefs.SetFloat("soundMult", soundMult);
        PlayerPrefs.SetFloat("musicMult", musicMult);
    }

    public void ChangeCanvas(string canvaName)
    {
        SoundManager.current.uiClick2();
        switch (canvaName)
        {
            case "HowTo":
                mainMenu.gameObject.SetActive(false);
                mmOBJ.SetActive(false);
                howToMenu.gameObject.SetActive(true);
                htOBJ.SetActive(true);
                settingMenu.gameObject.SetActive(false);
                seOBJ.SetActive(false);
                break;

            case "Setting":
                mainMenu.gameObject.SetActive(false);
                mmOBJ.SetActive(false);
                howToMenu.gameObject.SetActive(false);
                htOBJ.SetActive(false);
                settingMenu.gameObject.SetActive(true);
                seOBJ.SetActive(true);
                break;

            default:
                mainMenu.gameObject.SetActive(true);
                mmOBJ.SetActive(true);
                howToMenu.gameObject.SetActive(false);
                htOBJ.SetActive(false);
                settingMenu.gameObject.SetActive(false);
                seOBJ.SetActive(false);
                break;
        }
    }

	#region HowTo
	public void previousHowToPage()
	{
        if ((actualPageHowTo - 1) > 0)
        {
            ChangePageHowTo(actualPageHowTo - 1);
            actualPageHowTo--;
        }
    }

    public void nextHowToPage()
    {
		if ((actualPageHowTo + 1) <= 7)
		{
            ChangePageHowTo(actualPageHowTo + 1);
            actualPageHowTo++;
        }
    }

    private void ChangePageHowTo(int pageToShow)
	{
        SoundManager.current.uiClick();
        Debug.Log(pageToShow);
		switch (pageToShow)
		{
            case 2:
                p1.SetActive(false);
                p2.SetActive(true);
                p3.SetActive(false);
                p4.SetActive(false);
                p5.SetActive(false);
                p6.SetActive(false);
                p7.SetActive(false);
                btnPREV.SetActive(true);
                btnNEXT.SetActive(true);
                break;

            case 3:
                p1.SetActive(false);
                p2.SetActive(false);
                p3.SetActive(true);
                p4.SetActive(false);
                p5.SetActive(false);
                p6.SetActive(false);
                p7.SetActive(false);
                btnPREV.SetActive(true);
                btnNEXT.SetActive(true);
                break;

            case 4:
                p1.SetActive(false);
                p2.SetActive(false);
                p3.SetActive(false);
                p4.SetActive(true);
                p5.SetActive(false);
                p6.SetActive(false);
                p7.SetActive(false);
                btnPREV.SetActive(true);
                btnNEXT.SetActive(true);
                break;

            case 5:
                p1.SetActive(false);
                p2.SetActive(false);
                p3.SetActive(false);
                p4.SetActive(false);
                p5.SetActive(true);
                p6.SetActive(false);
                p7.SetActive(false);
                btnPREV.SetActive(true);
                btnNEXT.SetActive(true);
                break;

            case 6:
                p1.SetActive(false);
                p2.SetActive(false);
                p3.SetActive(false);
                p4.SetActive(false);
                p5.SetActive(false);
                p6.SetActive(true);
                p7.SetActive(false);
                btnPREV.SetActive(true);
                btnNEXT.SetActive(true);
                break;

            case 7:
                p1.SetActive(false);
                p2.SetActive(false);
                p3.SetActive(false);
                p4.SetActive(false);
                p5.SetActive(false);
                p6.SetActive(false);
                p7.SetActive(true);
                btnPREV.SetActive(true);
                btnNEXT.SetActive(false);
                break;

            default:
                p1.SetActive(true);
                p2.SetActive(false);
                p3.SetActive(false);
                p4.SetActive(false);
                p5.SetActive(false);
                p6.SetActive(false);
                p7.SetActive(false);
                btnPREV.SetActive(false);
                btnNEXT.SetActive(true);
                break;
		}
	}
	#endregion

	#region Simple Main Btn
	public void PlayTheGame()
    {
        SoundManager.current.stopMusic();
        FadeIn.startToFade = true;
        StartCoroutine(LoadAsyncScene());
    }


    IEnumerator LoadAsyncScene()
    {
        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.
        yield return new WaitForSeconds(1.5f);
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("GameScene");

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }

    public void HowTo()
    {
        ChangeCanvas("HowTo");
        ChangePageHowTo(1);
    }

    public void Setting()
    {
        ChangeCanvas("Setting");
    }

    public void QuitTheGame()
    {
        Application.Quit();
    }
	#endregion
}
