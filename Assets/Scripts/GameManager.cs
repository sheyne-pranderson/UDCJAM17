﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace roguelikesnake
{
	public class GameManager : MonoBehaviour
	{
		#region properties
		[Header("Player")]
		public CharacterController cc;

		public GameEvents current;
		public GameObject PlayerPrefab;
		public int actualHp = 10;
		public int maxHp = 10;
		public int nbrBrokenTile = 1;

		public int actualGold = 0;
		public float defaultTimeScale = 1f;
		public float timeScale = 1f;

		[Header("Actual level")]
		public RoomMapManager roomMapManager;
		public Color[] randomColors;
		public GameObject fog;
		public GameObject[] allTiles;
		public List<GameObject> allEntities = new List<GameObject>();
		public GameObject enemy;
		public GameObject money;
		public GameObject potion;
		public GameObject door;
		public GameObject portalGate;
		public GameObject merchant;

		[Header("level progression")]
		private bool isNExtLevel = false;
		private int basePoint = 1;
		private int[] goals			= { 15, 80, 288, 900, 1890, 3276, 5880, 8856, 11070, 20800, 33000 };
		private int[] mapSizes		= { 50, 80, 120, 150, 200, 250, 300, 350, 400, 450, 500 };
		private int[] maxRoomNumber	= { 5, 8, 12, 15, 18, 21, 24, 27, 30, 40, 50 };
		private int[] minRoomSizes	= { 5, 5, 10, 10, 15, 15, 20, 20, 20, 20, 25 };
		private int[] maxRoomSizes	= { 8, 12, 12, 20, 20, 25, 25, 30, 30, 35, 35 };
		public int actualDifficultyLevel = 1;
		public int globalScore = 0;
		public int actualLevelScore = 0;
		public int minScore = 0;
		public int maxScore;
		public float speedAddedAtNextLevel = 0.1f;

		[Header("Game Mode")]
		public bool isAdventureMode = true;
		public float percentLuckMerchantRoom = 5f;
		public float percentLuckMoneyRoom = 20f;
		public float percentLuckEnemyRoom = 30f;
		public float percentLuckPotionRoom = 20f;
		public float percentLuckMixtRoom = 25f;

		[Header("GameOver/Win")]
		public GameObject GameOverUI;
		public Text GameOverScore;
		public GameObject WinUI;
		public Text WinScore;

		[Header("Shop")]
		public GameObject shopUI;
		public Marchand actualMarchand;
		public Button buyHP;
		public Button buySlow;
		public Button buyTile;
		public Button buyRespawn;

		private List<Tile> connectedCorridorTiles = new List<Tile>();
		#endregion

		#region lifeCycle
		private void OnEnable()
		{
			randomColors = setRandomColors();
			maxScore = goals[0];
		}

		private void Start()
		{
			GameEvents.current.OnLevelFullyReady += referencingAllTile;
			GameEvents.current.OnPlayerCollideWallOrVoid += GameOverMyDudes;
			GameEvents.current.OnPlayerWin += YouWonMyDudes;
			Time.timeScale = timeScale;
			fog.GetComponent<Renderer>().material.SetColor("_BaseColor", randomColors[1]);
		}

		// Update is called once per frame
		private void Update()
		{
			if (!cc && GameObject.FindGameObjectWithTag("Player"))
				cc = GameObject.FindGameObjectWithTag("Player").GetComponent<roguelikesnake.CharacterController>();
		}

		private void OnDisable()
		{
			GameEvents.current.OnLevelFullyReady -= referencingAllTile;
		}
		#endregion

		private void referencingAllTile()
		{
			allTiles = GameObject.FindGameObjectsWithTag("FloorTile");
			placeEntityRandomTile();
		}

		private IEnumerator waitABit()
		{
			yield return new WaitForEndOfFrame();
			yield return new WaitForSeconds(2);
			cc.isPause = false;
		}

		public void placeEntityRandomTile()
		{
			int e = Random.Range(0, allTiles.Length);
			int m = Random.Range(0, allTiles.Length);
			int po = Random.Range(0, allTiles.Length);
			int pl = Random.Range(0, allTiles.Length);
			Vector3 newEnemyPosition = LocationOkToSpawn (new Vector3(allTiles[e].transform.position.x, 1, allTiles[e].transform.position.z));
			Vector3 newMoneyPosition = LocationOkToSpawn( new Vector3(allTiles[m].transform.position.x, 1, allTiles[m].transform.position.z));
			Vector3 newPotionPosition = LocationOkToSpawn( new Vector3(allTiles[po].transform.position.x, 1, allTiles[po].transform.position.z));
			Vector3 newPlayerPosition = new Vector3(allTiles[pl].transform.position.x, 1, allTiles[pl].transform.position.z);

			if (!isNExtLevel)
			{
				resetAllValue();
				GameEvents.current.UpgradingUI();
			}
			GameObject ennemyObj = Instantiate(enemy, newEnemyPosition, transform.rotation);
			GameObject moneyObj = Instantiate(money, newMoneyPosition, transform.rotation);
			GameObject potionObj = Instantiate(potion, newPotionPosition, transform.rotation);
			allEntities.Add(ennemyObj);
			allEntities.Add(moneyObj);
			allEntities.Add(potionObj);
			instantiateDoors();
			GameObject player = Instantiate(PlayerPrefab, newPlayerPosition, transform.rotation);
			player.name = "Player";
			GameObject portalLevel = Instantiate(portalGate, new Vector3(newPlayerPosition.x, 0, newPlayerPosition.z), transform.rotation);
			portalLevel.name = "Gate";
			cc = player.GetComponent<CharacterController>();
			PlayerCamera pc = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerCamera>();
			pc.targetFollow = cc.transform;
			isNExtLevel = false;

			if(isAdventureMode)
				instantiateFixedEntitiesInRooms();

			setPlayerSurroundingVisible();
			StartCoroutine(waitABit());
		}

		public Vector3 LocationOkToSpawn(Vector3 posToCheck)
		{
			Vector3 pos = posToCheck;
			bool test = true;
			do
			{
				foreach (GameObject entity in allEntities)
				{
					if (pos.x == entity.transform.position.x && pos.z == entity.transform.position.z)
					{
						int r = Random.Range(0, allTiles.Length);
						pos = new Vector3(allTiles[r].transform.position.x, 1, allTiles[r].transform.position.z);
					}
				}
				test = false;
			}
			while (test);
			return pos;
		}

		public void instantiateFixedEntitiesInRooms()
        {
			int MoyenneRoomSize = (minRoomSizes[actualDifficultyLevel - 1] + maxRoomSizes[actualDifficultyLevel - 1]) / 2;
			int MoyenneRoomSizeSquared = MoyenneRoomSize * MoyenneRoomSize;
			int NbrProp = MoyenneRoomSizeSquared / 15;

			//Theme rooms. 1 = full piece, 2 = full potion, 3 = full monsters, 4 = mixt everything at random

			foreach (Room room in roomMapManager.mapInstance._rooms)
            {
				int random = Random.Range(0, 101);
				List<GameObject> fixedEntities = new List<GameObject>();
				if(random < percentLuckMerchantRoom) //Merchant
                {
					GameObject marchandObject = Instantiate(merchant, new Vector3((int)room.transform.position.x, 1, (int)room.transform.position.z), transform.rotation);
					marchandObject.GetComponent<GlobalEntity>().isLinkedToRoom = true;
					fixedEntities.Add(marchandObject);
				}
				else if(random < percentLuckMerchantRoom + percentLuckMoneyRoom) //Money
                {
					for (int i = 0; i < NbrProp; i++)
					{
						int j = 0;
						while (j < 100)
						{
							int randomTile = Random.Range(0, room.roomTiles.Count);
							foreach (GameObject entity in allEntities)
							{
								if (entity.transform.position.x != room.roomTiles[randomTile].transform.position.x
									|| entity.transform.position.z != room.roomTiles[randomTile].transform.position.z)
								{
									GameObject moneyObj = Instantiate(money, new Vector3(room.roomTiles[randomTile].transform.position.x, 1, room.roomTiles[randomTile].transform.position.z), transform.rotation);
									moneyObj.GetComponent<GlobalEntity>().isLinkedToRoom = true;
									fixedEntities.Add(moneyObj);
									j = 100;
									break;
								}
							}
							j++;
						}
					}
				}
				else if (random < percentLuckMerchantRoom + percentLuckMoneyRoom + percentLuckEnemyRoom) //Enemy
				{
					for (int i = 0; i < NbrProp; i++)
					{
						int j = 0;
						while (j < 100)
						{
							int randomTile = Random.Range(0, room.roomTiles.Count);
							foreach (GameObject entity in allEntities)
							{
								if (entity.transform.position.x != room.roomTiles[randomTile].transform.position.x
									|| entity.transform.position.z != room.roomTiles[randomTile].transform.position.z)
								{
									GameObject moneyObj = Instantiate(enemy, new Vector3(room.roomTiles[randomTile].transform.position.x, 1, room.roomTiles[randomTile].transform.position.z), transform.rotation);
									moneyObj.GetComponent<GlobalEntity>().isLinkedToRoom = true;
									fixedEntities.Add(moneyObj);
									j = 100;
									break;
								}
							}
							j++;
						}
					}
				}
				else if (random < percentLuckMerchantRoom + percentLuckMoneyRoom + percentLuckEnemyRoom + percentLuckPotionRoom) //Potion
				{
					for (int i = 0; i < NbrProp; i++)
					{
						int j = 0;
						while (j < 100)
						{
							int randomTile = Random.Range(0, room.roomTiles.Count);
							foreach (GameObject entity in allEntities)
							{
								if (entity.transform.position.x != room.roomTiles[randomTile].transform.position.x
									|| entity.transform.position.z != room.roomTiles[randomTile].transform.position.z)
								{
									GameObject moneyObj = Instantiate(potion, new Vector3(room.roomTiles[randomTile].transform.position.x, 1, room.roomTiles[randomTile].transform.position.z), transform.rotation);
									moneyObj.GetComponent<GlobalEntity>().isLinkedToRoom = true;
									fixedEntities.Add(moneyObj);
									j = 100;
									break;
								}
							}
							j++;
						}
					}
				}
				else //Mixed
				{
					for (int i = 0; i < NbrProp; i++)
					{
						int j = 0;
						while (j < 100)
						{
							int randomTile = Random.Range(0, room.roomTiles.Count);
							foreach (GameObject entity in allEntities)
							{
								if (entity.transform.position.x != room.roomTiles[randomTile].transform.position.x
									|| entity.transform.position.z != room.roomTiles[randomTile].transform.position.z)
								{


									GameObject moneyObj = null;

									int whatToSpawn = Random.Range(0, 3);
									if (whatToSpawn == 0)
									{
										moneyObj = Instantiate(money, new Vector3(room.roomTiles[randomTile].transform.position.x, 1, room.roomTiles[randomTile].transform.position.z), transform.rotation);
									}
									else if (whatToSpawn == 1)
									{
										moneyObj = Instantiate(potion, new Vector3(room.roomTiles[randomTile].transform.position.x, 1, room.roomTiles[randomTile].transform.position.z), transform.rotation);
									}
									else
									{
										moneyObj = Instantiate(enemy, new Vector3(room.roomTiles[randomTile].transform.position.x, 1, room.roomTiles[randomTile].transform.position.z), transform.rotation);
									}

									moneyObj.GetComponent<GlobalEntity>().isLinkedToRoom = true;
									fixedEntities.Add(moneyObj);
									j = 100;
									break;
								}
							}
							j++;
						}
					}
				}

				foreach(GameObject entity in fixedEntities)
                {
					allEntities.Add(entity);
				}

			}
			//loop throught all the rooms
			//Foreach rooms instanciate at random money or potions or monsters

			//Check if the object is instanciated on the playerPosition and put it somewhere else



        }

		public void setPlayerSurroundingVisible()
        {
			Tile tile = getPlayerTargetTile();
			List<Tile> tiles = new List<Tile>();
			List<Tile> finaleTiles = new List<Tile>();
			connectedCorridorTiles = new List<Tile>();
			bool isPlayerInRoom = true;
			if(tile != default(Tile))
            {
				Corridor corridor = roomMapManager.getCorridorByTile(tile);
				if(corridor != default(Corridor))
                {
					isPlayerInRoom = false;
					//tiles = corridor.corridorTiles;
					//Not enough, corridors can be connected to other corridors. We will loop through every corridor tiles
					//And keep looking at if there are any corridor tiles connected that are not in the list to add them.
					getAllConnectedCorridorsTilesByTile(tile);

					tiles = connectedCorridorTiles;
				}
				else
                {
					Room room = roomMapManager.getRoomByTile(tile);
					if(room != default(Room))
                    {
						isPlayerInRoom = true;
						tiles = room.roomTiles;
                    }
                }
            }

			//Get also the first tiles of corridor with doors connected to player
			foreach (Tile singleTile in tiles)
			{
				finaleTiles.Add(singleTile);
				if (isPlayerInRoom && singleTile.ifRoomTileThisIsTheCorridorTileConnectedTo)
                {
					finaleTiles.Add(singleTile.ifRoomTileThisIsTheCorridorTileConnectedTo);
				}
			}

			foreach (Tile singleTile in finaleTiles)
            {
				if (!singleTile.isVisible)
				{
					singleTile.setVisible();
					singleTile.StartCoroutine(singleTile.animateAppearing());
				}
			}

			setObjectsVisibleIfPlayerCanSee();
		}

		public void getAllConnectedCorridorsTilesByTile(Tile tile)
        {
			if(!connectedCorridorTiles.Contains(tile))
				connectedCorridorTiles.Add(tile);

			foreach(Tile aroundTile in tile.getAroundTiles())
            {
				if(aroundTile.CompareTag("connexion") && !connectedCorridorTiles.Contains(aroundTile))
                {
					connectedCorridorTiles.Add(aroundTile);
					getAllConnectedCorridorsTilesByTile(aroundTile);
				}
            }
		}

		public void setObjectsVisibleIfPlayerCanSee()
        {
			foreach(GameObject Entity in allEntities)
            {
				if (!Entity.GetComponent<GlobalEntity>().isVisible)
				{
					RaycastHit hit;
					if (Physics.Raycast(Entity.transform.position, Entity.transform.TransformDirection(Vector3.down), out hit, Mathf.Infinity))
					{
						if ((hit.collider.CompareTag("FloorTile") || hit.collider.CompareTag("connexion")))
						{
							if (hit.collider.transform.GetChild(0).GetComponent<MeshRenderer>().enabled)
							{
								GlobalEntity ge = Entity.GetComponent<GlobalEntity>();
								ge.setVisible();
								ge.StartCoroutine(ge.animateAppearing());
							}
						}
					}
				}
			}
        }

		public Tile getPlayerTargetTile()
        {
			GameObject Player = GameObject.FindGameObjectWithTag("Player");
			CharacterController cc = Player.GetComponent<CharacterController>();
			//Si le jeu a commencé le joueur compute les tiles lui même on a donc pas besoin de raycast
			if (cc.targetTile)
            {
				//Si le joueur est sur une tile dans une piece et qu'on rentre là dedans, ça veut dire qu'il passe une porte menant vers un corridor
				//Donc on va passer une tile de corridor
				if (cc.actualTile.CompareTag("FloorTile"))
                {
					return cc.targetTile.GetComponent<Tile>();
                }
				//Si le joueur est sur une tile dans un corridor et qu'on rentre là dedans, ça veut dire qu'il passe une porte menant vers une piece
				//Donc on va passer la tile de la piece connectée à la tile de la porte qu'on est entrain de passer
				else
				{
					return cc.targetTile.GetComponent<Tile>().ifCorridorTileThisIsTheRoomTileConnectedTo;
				}
			}
			RaycastHit hit;
			if (Physics.Raycast(Player.transform.position, Player.transform.TransformDirection(Vector3.down), out hit, Mathf.Infinity))
			{
				if ((hit.collider.CompareTag("FloorTile") || hit.collider.CompareTag("connexion")))
				{
					return hit.collider.GetComponent<Tile>();
				}
			}

			return default(Tile);
		}

		public void instantiateDoors()
        {
			foreach(Tile tile in roomMapManager.getAllTilesConnectedToRooms())
            {
				Quaternion rotation = Quaternion.identity;
				if (tile.ifCorridorTileThisIsTheRoomTileConnectedTo != null)
                {
					Vector3 relativePos = tile.ifCorridorTileThisIsTheRoomTileConnectedTo.transform.position - tile.transform.position;
					rotation = Quaternion.LookRotation(relativePos, Vector3.up);
				}
				GameObject Door = Instantiate(door, new Vector3(tile.transform.position.x, tile.transform.position.y +1, tile.transform.position.z), rotation);
				allEntities.Add(Door);
			}
        }

		#region restart
		public void restartGame()
		{
			foreach (GameObject item in allEntities)
			{
				Destroy(item);
			}
			randomColors = setRandomColors();
			fog.GetComponent<Renderer>().material.SetColor("_BaseColor", randomColors[1]);
			allEntities = new List<GameObject>();
			StartCoroutine(restartGameCR());
		}

		private IEnumerator restartGameCR()
		{
			cc.gameObject.SetActive(false);
			Destroy(cc.gameObject);
			Destroy(GameObject.Find("Gate"));
			GameOverUI.SetActive(false);
			yield return new WaitForEndOfFrame();
			roomMapManager.RestartGameProg();
		}

		private IEnumerator restartMap()
		{
			foreach (GameObject item in allEntities)
			{
				Destroy(item);
			}
			randomColors = setRandomColors();
			fog.GetComponent<Renderer>().material.SetColor("_BaseColor", randomColors[1]);
			allEntities = new List<GameObject>();
			Destroy(cc.gameObject);
			Destroy(GameObject.Find("Gate"));
			yield return new WaitForEndOfFrame();
			roomMapManager.RestartGameProg();
		}
		#endregion

		public void breakADoor(Door Door)
		{
			AddScore();
			allEntities.Remove(Door.gameObject);
			StartCoroutine(Door.destroyTheDoor());
			//TODO Reveal the cubes behind it
			setPlayerSurroundingVisible();
		}

		private void resetAllValue()
		{
			actualHp = 10;
			maxHp = 10;
			actualDifficultyLevel = 1;
			nbrBrokenTile = 5;
			actualGold = 0;
			timeScale = defaultTimeScale;
			ChangeLevelSetup();
			ResetScoreWholeGame();
		}

		public Color[] setRandomColors()
		{
			float H, S, V;
			Color top = Random.ColorHSV(0f, 1f, 0.6f, 0.7f, 0.3f, 0.4f);
			Color.RGBToHSV(top, out H, out S, out V);
			Color bot = Color.HSVToRGB(H + 0.15f, S, 1);
			Color[] valRet = { top, bot };
			return valRet;
		}

		#region Merchant
		public void ShowShopUI()
		{
			SetShopBtn();
			shopUI.SetActive(true);
			SoundManager.current.marchandHello();
		}
		
		public void HideShopUI()
		{
			GameEvents.current.ClosedShop(actualMarchand);
			shopUI.SetActive(false);
			SoundManager.current.marchandGoodbye();
			SoundManager.current.uiClick();
			cc.isPause = false;
		}

		public void SetShopBtn()
		{
			if (actualGold >= (maxHp - actualHp))
			{
				buyHP.interactable = true;
			}
			if (actualGold >= 75)
			{
				buyTile.interactable = true;
			}
			if (actualGold >= 100)
			{
				buyRespawn.interactable = true;
			}
			if (actualGold >= 200)
			{
				buySlow.interactable = true;
			}
		}

		public void ShopHPBtn()
		{
			SoundManager.current.uiClick();
			actualGold -= (maxHp - actualHp);
			actualHp = maxHp;
		}

		public void ShopSlowBtn()
		{
			SoundManager.current.uiClick();
			actualGold -= 200;
			if (Time.timeScale - 0.5f < 1)
			{
				Time.timeScale = 1;
				timeScale = 1;
			}
			else
            {
				timeScale -= 0.5f;
				Time.timeScale -= 0.5f;
			}
		}

		public void ShopTiletn()
		{
			SoundManager.current.uiClick();
			actualGold -= 75;
			nbrBrokenTile /= 2;
			List<GameObject> oldTiles = new List<GameObject>();
			
			foreach (GameObject tileObj in cc.oldTiles)
            {
				oldTiles.Add(tileObj);
			}
			foreach(GameObject tileObj in oldTiles)
            {
				Tile tile = tileObj.GetComponent<Tile>();
				tile.StartCoroutine(tile.reappear(cc));
			}
		}

		public void ShopRespawnBtn()
		{
			SoundManager.current.uiClick();
			actualGold -= 100;
			instantiateFixedEntitiesInRooms();
			setPlayerSurroundingVisible();
		}

		#endregion

		#region Score
		public void AddScore()
		{
			globalScore += basePoint * actualDifficultyLevel;
			actualLevelScore += basePoint * actualDifficultyLevel;
			IncrementBrokenTileNumber();
		}

		private void AddScoreInternal()
		{
			globalScore += basePoint * actualDifficultyLevel;
			actualLevelScore += basePoint * actualDifficultyLevel;
		}

		public void KillEnemy()
		{
			Debug.Log("Damage " + actualHp);
			AddScoreInternal();
			AddScoreInternal();
			actualHp--;
			IncrementBrokenTileNumber();
		}

		private void IncrementBrokenTileNumber()
		{
			nbrBrokenTile++;
		}

		public void ResetScoreForNextLevel()
		{
			actualLevelScore = 0;
			minScore = 0;
			maxScore = goals[actualDifficultyLevel - 1];
		}

		public void ResetScoreWholeGame()
		{
			globalScore = 0;
			actualLevelScore = 0;
			minScore = 0;
			maxScore = goals[0];
		}
		#endregion

		#region Change Level
		public void GoNextLevel()
		{
			SoundManager.current.endGameGateTravel();
			SoundManager.current.stopMusic();
			actualDifficultyLevel++;
			if (actualDifficultyLevel > 11)
			{
				GameEvents.current.PlayerWin();
			} 
			else
			{
				nbrBrokenTile = 5;
				ResetScoreForNextLevel();
				isNExtLevel = true;
				ChangeMaxHp();
				GameEvents.current.UpgradingUI();
				ChangeLevelSetup();
				restartGame();
				timeScale += speedAddedAtNextLevel;
				Time.timeScale = timeScale;
			}
		}

		public void ChangeMaxHp()
		{
			maxHp = Mathf.RoundToInt(maxHp * 1.5f);
		}

		public void ChangeLevelSetup()
		{
			roomMapManager.MapSizeX = mapSizes[actualDifficultyLevel - 1];
			int sizezz = Mathf.RoundToInt(roomMapManager.MapSizeX / 1.7f);
			if (sizezz % 2 != 0)
			{
				sizezz++;
			}
			roomMapManager.MapSizeZ = sizezz;
			roomMapManager.MaxRooms = maxRoomNumber[actualDifficultyLevel - 1];
			roomMapManager.MinRoomSize = minRoomSizes[actualDifficultyLevel - 1];
			roomMapManager.MaxRoomSize = maxRoomSizes[actualDifficultyLevel - 1];
		}
		#endregion

		#region Win/Loose
		public void GameOverMyDudes()
		{
			cc.isPause = true;
			actualDifficultyLevel = 1;
			ChangeLevelSetup();
			Time.timeScale = defaultTimeScale;
			GameOverScore.text = "Score : " + globalScore;
			StartCoroutine(timeBeforGameOver(1));
		}

		public IEnumerator timeBeforGameOver(int s)
		{
			yield return new WaitForSeconds(s);
			GameOverUI.SetActive(true);
		}

		public void YouWonMyDudes()
		{
			cc.isPause = true;
			actualDifficultyLevel = 1;
			ChangeLevelSetup();
			Time.timeScale = defaultTimeScale;
			speedAddedAtNextLevel = 0.1f;
			WinScore.text = "Score : " + globalScore;
			WinUI.SetActive(true);
		}

		public void GoBackToMainMenu()
		{
			SceneManager.LoadScene("MainMenuScene", LoadSceneMode.Single);
		}
		#endregion
	}
}