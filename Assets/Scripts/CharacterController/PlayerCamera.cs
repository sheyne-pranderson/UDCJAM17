﻿using UnityEngine;
using System.Collections;
using roguelikesnake;
using UnityEngine.InputSystem;

namespace roguelikesnake
{
    public class PlayerCamera : MonoBehaviour
    {
        // General Vars
        public Transform targetFollow;
        public Controls controls;
        private bool lookAround = true;

        // For SmoothDampFollow
        public Vector3 followDefaultDistance = new Vector3(0f, 4f, -4f);
        public float followDistanceDamp = 0.4f;
        public Vector3 followVelocity = Vector3.one;

        // For Camera Orbit
        public float orbitDistance = 20.0f;
        public float orbitDamp = 5.0f;

        // For Zooming Field Of View
        public float FOVmin = 50.0f;
        public float FOVmax = 100.0f;
        public float mouseWheelSpeed = 4.0f;

        public bool rightClickPressed = false;

        public float canvasSpeed = 8f;

        private void Awake()
        {
            controls = new Controls();
        }

        void OnEnable()
        {
            controls.PlayerGame.Scroll.performed += HandleScroll;
            controls.PlayerGame.Scroll.Enable();
            controls.PlayerGame.CameraMovementsX.performed += HandleCameraX;
            controls.PlayerGame.CameraMovementsX.Enable();
            controls.PlayerGame.CameraMovementY.performed += HandleCameraY;
            controls.PlayerGame.CameraMovementY.Enable();
            controls.PlayerGame.RightClick.started += HandleRightClickStarted;
            controls.PlayerGame.RightClick.canceled += HandleRightClickStopped;
            controls.PlayerGame.RightClick.Enable();
        }

        void OnDisable()
        {
            controls.PlayerGame.Scroll.performed -= HandleScroll;
            controls.PlayerGame.Scroll.Disable();
            controls.PlayerGame.CameraMovementsX.performed -= HandleCameraX;
            controls.PlayerGame.CameraMovementsX.Disable();
            controls.PlayerGame.CameraMovementY.performed -= HandleCameraY;
            controls.PlayerGame.CameraMovementY.Disable();
            controls.PlayerGame.RightClick.started -= HandleRightClickStarted;
            controls.PlayerGame.RightClick.canceled -= HandleRightClickStopped;
            controls.PlayerGame.RightClick.Disable();
        }

        public void HandleCameraY(InputAction.CallbackContext context)
        {
            if (rightClickPressed)
            {
                float addY = context.ReadValue<float>();
                addY = addY * canvasSpeed * Time.fixedDeltaTime;
                Vector3 wantedPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z + addY);
                transform.position = Vector3.Lerp(transform.position, wantedPosition, Time.deltaTime * orbitDamp);
            }
        }
        public void HandleCameraX(InputAction.CallbackContext context)
        {
            if (rightClickPressed)
            {
                float addX = context.ReadValue<float>();
                addX = addX * canvasSpeed * Time.fixedDeltaTime;
                Vector3 wantedPosition = new Vector3(transform.position.x + addX, transform.position.y, transform.position.z);
                transform.position = Vector3.Lerp(transform.position, wantedPosition, Time.deltaTime * orbitDamp);
            }
        }
        public void HandleRightClickStopped(InputAction.CallbackContext context)
        {
            rightClickPressed = false;
        }
        public void HandleRightClickStarted(InputAction.CallbackContext context)
        {
            rightClickPressed = true;
        }

        public void HandleScroll(InputAction.CallbackContext context)
        {
            float scroll = context.ReadValue<Vector2>().y;
            if (scroll > 0)
            {
                transform.GetChild(0).GetComponent<Camera>().fieldOfView = transform.GetChild(0).GetComponent<Camera>().fieldOfView - mouseWheelSpeed;
                if (transform.GetChild(0).GetComponent<Camera>().fieldOfView <= FOVmin) { transform.GetChild(0).GetComponent<Camera>().fieldOfView = FOVmin; }
            }
            else if (scroll < 0)
            {
                transform.GetChild(0).GetComponent<Camera>().fieldOfView = transform.GetChild(0).GetComponent<Camera>().fieldOfView + mouseWheelSpeed;
                if (transform.GetChild(0).GetComponent<Camera>().fieldOfView >= FOVmax) { transform.GetChild(0).GetComponent<Camera>().fieldOfView = FOVmax; }
            }
        }

        void FixedUpdate()
        {
            if(!rightClickPressed)
            {
                SmoothDampFollow();
           }
        }

        void SmoothDampFollow()
        {
            if (!targetFollow)
            {
                return;
            }
            else
            {
                Vector3 wantedPosition = targetFollow.position + (targetFollow.rotation * followDefaultDistance);
                transform.position = Vector3.SmoothDamp(transform.position, wantedPosition, ref followVelocity, followDistanceDamp);
                transform.LookAt(targetFollow, targetFollow.up);

            }
        }
    }
}