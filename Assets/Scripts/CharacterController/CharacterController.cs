﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace roguelikesnake
{
	public class CharacterController : MonoBehaviour
	{
		public Controls controls;

		//The axis input from the player
		public Vector2 moveAxis = new Vector2();
		public Transform model;
		public Vector2 lastAxis = new Vector2(); //will be used to prevent the player to move backward on it's own trails
		public float speed = 60f;
		public bool doesTileChanged = false; //Track wether the player walk on a new tile
		public bool isTargetReached = true;
		public float modelRotationSpeed = 0.01f;

		public List<GameObject> oldTiles = new List<GameObject>();

		public GameObject actualTile = null;
		public GameObject leftTile = null;
		public GameObject rightTile = null;
		public GameObject upTile = null;
		public GameObject downTile = null;
		public GameObject targetTile = null;
		private GameManager GM;

		private Vector3 actualPosition;
		private float timeLerped = 0f;

		public Vector3 target;

		public bool isPause = true;
		private void OnEnable()
		{
			controls.PlayerGame.Move.performed += HandleMove;
			controls.PlayerGame.Move.Enable();
		}

		private void Awake()
		{
			model = transform.GetChild(0);
			controls = new Controls();
		}

		private void Start()
		{
			GameEvents.current.OnLevelFullyReady += OnRunTheGame;
		}

        private void Update()
        {
        }

        private void OnRunTheGame()
		{
			isPause = false;
		}

		private void OnTriggerEnter(Collider collider)
		{
			if (collider.transform.CompareTag("Portal"))
			{
				GM.GoNextLevel();
			}
		}

		private void OnDisable()
		{
			controls.PlayerGame.Move.performed -= HandleMove;
			controls.PlayerGame.Move.Disable();
		}

		public void HandleMove(InputAction.CallbackContext context)
		{
			moveAxis = context.ReadValue<Vector2>();
		}

		private void FixedUpdate()
		{
			if (!isPause)
			{
				if (isTargetReached)
				{
					isTargetReached = false;
					setMoveAxis();
					getActualTile();
					getAroundTiles();
					selectTargetTile();
				}
				if (!isTargetReached)
				{
					movePlayer();
					if (targetTile && actualTile)
					{
						Vector3 direction = new Vector3(targetTile.transform.position.x, actualTile.transform.position.y, targetTile.transform.position.z) - actualTile.transform.position;
						Quaternion toRotation = Quaternion.LookRotation(direction, model.transform.up);
						model.transform.rotation = Quaternion.Lerp(model.transform.rotation, toRotation, modelRotationSpeed * Time.deltaTime);
					}
				}
				getIsTargetReached();
			}
		}

		private void getIsTargetReached()
		{
			if (targetTile && targetTile.transform.position.x == transform.position.x && targetTile.transform.position.z == transform.position.z && targetTile != actualTile)
			{
				isTargetReached = true;
			}
		}

		private void setMoveAxis()
		{
			//Prevent the player to move backward
			if (lastAxis.x != 0 || lastAxis.y != 0)
			{
				if (moveAxis.normalized == -lastAxis.normalized)
				{
					if (Mathf.Abs(lastAxis.x) > Mathf.Abs(lastAxis.y))
					{
						moveAxis = new Vector2(lastAxis.x, 0);
					}
					else
					{
						moveAxis = new Vector2(0, lastAxis.y);
					}
				}
				else
				{
					if (Mathf.Abs(moveAxis.x) > Mathf.Abs(moveAxis.y))
					{
						moveAxis = new Vector2(moveAxis.x, 0);
					}
					else
					{
						moveAxis = new Vector2(0, moveAxis.y);
					}
				}
			}
			else
			{
				if (Mathf.Abs(moveAxis.x) > Mathf.Abs(moveAxis.y))
				{
					moveAxis = new Vector2(moveAxis.x, 0);
				}
				else
				{
					moveAxis = new Vector2(0, moveAxis.y);
				}
			}
		}

		private void selectTargetTile()
		{
			RaycastHit hit;
			if (actualTile && (moveAxis.x != 0 || moveAxis.y != 0))
			{
				Vector2 movement = moveAxis.normalized;
				lastAxis = movement;
				// Does the ray intersect any objects excluding the player layer
				if (Physics.Raycast(actualTile.transform.position, new Vector3(movement.x, 0, movement.y), out hit, 0.8f))
				{
					Debug.DrawRay(transform.position, new Vector3(movement.x, 0, movement.y) * hit.distance, Color.yellow);
					if (hit.collider.CompareTag("FloorTile") || hit.collider.CompareTag("connexion"))
					{
						targetTile = hit.collider.gameObject;
					}
				}
				else //No target at all
				{
					dispatchWallCollisionOrVoidEvent();
				}
			}
			else
			{
				if (leftTile)
				{
					targetTile = leftTile;
				}
				else if (rightTile)
				{
					targetTile = rightTile;
				}
				else if (upTile)
				{
					targetTile = upTile;
				}
				else if (downTile)
				{
					targetTile = downTile;
				}
				moveAxis = -new Vector2(actualTile.transform.position.x - targetTile.transform.position.x, actualTile.transform.position.z - targetTile.transform.position.z);
			}
		}

		private void dispatchWallCollisionOrVoidEvent()
		{
			GameEvents.current.PlayerCollideWallOrVoid();
			isPause = true;
			StartCoroutine(animatePlayerDeath());
		}

		public IEnumerator animatePlayerDeath()
        {
			GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerCamera>().targetFollow = actualTile.transform;
			float elapsedTime = 0f;
			float waitingTime = 60f;
			float timeBeforeMomentumFowardEnd = 0.5f;
			float positionY = transform.position.y;
			float fallSpeed = 5f;
			bool isNewLerp = false;
			float newPositionY = -300f;
			while (elapsedTime < waitingTime)
            {
				elapsedTime += Time.unscaledDeltaTime;
				positionY *= Time.unscaledDeltaTime * fallSpeed;

				if(!isNewLerp && elapsedTime > timeBeforeMomentumFowardEnd)
                {
					SoundManager.current.screamFall();
					isNewLerp = true;
					actualPosition = transform.position;
					elapsedTime = 0f;
					timeBeforeMomentumFowardEnd = 9999f;
				}


				if(elapsedTime < timeBeforeMomentumFowardEnd && !isNewLerp)
                {
					target = new Vector3(actualPosition.x + lastAxis.x * 1.2f, positionY, actualPosition.z + lastAxis.y * 1.2f);
					transform.position = Vector3.Lerp(actualPosition, target, (elapsedTime / timeBeforeMomentumFowardEnd));
				}else
                {
					target = new Vector3(actualPosition.x + lastAxis.x * 20f, newPositionY, actualPosition.z + lastAxis.y * 20f);
					transform.position = Vector3.Lerp(actualPosition, target, (elapsedTime / waitingTime));
				}

				yield return null;
			}

			yield return null;
        }

		private void movePlayer()
		{
			if (targetTile)
			{
				timeLerped += Time.fixedDeltaTime * speed;
				if(timeLerped > 1)
                {
					timeLerped = 1;
				}
				//transform.position = Vector3.MoveTowards(transform.position, new Vector3(targetTile.transform.position.x, transform.position.y, targetTile.transform.position.z), Time.deltaTime * speed);
				transform.position = Vector3.Lerp(actualPosition, new Vector3(targetTile.transform.position.x, transform.position.y, targetTile.transform.position.z), timeLerped / 1);
			}
		}

		private void getAroundTiles()
		{
			RaycastHit hit;
			if (actualTile && doesTileChanged)
			{
				doesTileChanged = false;
				// Does the ray intersect any objects excluding the player layer
				if (Physics.Raycast(actualTile.transform.position, actualTile.transform.TransformDirection(Vector3.left), out hit, 0.8f))
				{
					Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.left) * hit.distance, Color.yellow);
					if (hit.collider.CompareTag("FloorTile") || hit.collider.CompareTag("connexion"))
					{
						leftTile = hit.collider.gameObject;
					}
				}
				if (Physics.Raycast(actualTile.transform.position, actualTile.transform.TransformDirection(Vector3.right), out hit, 0.8f))
				{
					Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.right) * hit.distance, Color.yellow);
					if (hit.collider.CompareTag("FloorTile") || hit.collider.CompareTag("connexion"))
					{
						rightTile = hit.collider.gameObject;
					}
				}
				if (Physics.Raycast(actualTile.transform.position, actualTile.transform.TransformDirection(Vector3.forward), out hit, 0.8f))
				{
					Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
					if (hit.collider.CompareTag("FloorTile") || hit.collider.CompareTag("connexion"))
					{
						upTile = hit.collider.gameObject;
					}
				}
				if (Physics.Raycast(actualTile.transform.position, actualTile.transform.TransformDirection(Vector3.back), out hit, 0.8f))
				{
					Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.back) * hit.distance, Color.yellow);
					if (hit.collider.CompareTag("FloorTile") || hit.collider.CompareTag("connexion"))
					{
						downTile = hit.collider.gameObject;
					}
				}
			}
		}

		private void getActualTile()
		{
			RaycastHit hit;
			// Does the ray intersect any objects excluding the player layer
			if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, Mathf.Infinity))
			{
				Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.down) * hit.distance, Color.yellow);
				if ((hit.collider.CompareTag("FloorTile") || hit.collider.CompareTag("connexion")) && actualTile != hit.collider.gameObject)
				{
					if (actualTile != null)
					{
						if (oldTiles.Contains(hit.collider.gameObject))
						{
							dispatchWallCollisionOrVoidEvent();
						}
						else
						{
							oldTiles.Add(actualTile);
							if (!actualTile.GetComponent<Tile>())
							{
								actualTile.AddComponent<Tile>();
							}
							actualTile.GetComponent<Tile>().StartCoroutine(actualTile.GetComponent<Tile>().onPlayerExited(this));
							setRightNumberOfTrail();
						}
					}

					actualTile = hit.collider.gameObject;
					doesTileChanged = true;
					actualPosition = new Vector3(actualTile.transform.position.x, transform.position.y, actualTile.transform.position.z);
					timeLerped = 0f;
				}
			}
		}

		private void setRightNumberOfTrail()
		{
			if (!GM)
				GM = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();

			float maxTiles = GM.nbrBrokenTile;

			if (oldTiles.Count > maxTiles - 1)
			{
				Tile tile = oldTiles[0].GetComponent<Tile>();
				tile.StartCoroutine(tile.reappear(this));
			}
		}
	}
}