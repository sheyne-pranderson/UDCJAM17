﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
#if UNITY_EDITOR
[CustomEditor(typeof(RoomGeneration))]
public class CIroomGeneration : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		RoomGeneration room = (RoomGeneration)target;
		if (GUILayout.Button("Generate Room"))
		{
			room.GenerateRoom();
		}
		if (GUILayout.Button("DeleteCubes"))
		{
			room.DeleteAllGameObjectChildren();
		}
		if (GUILayout.Button("Generate Room (from primitive)"))
		{
			room.GenerateRoom();
		}
	}
}
#endif