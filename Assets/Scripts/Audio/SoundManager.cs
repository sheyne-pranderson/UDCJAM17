﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace roguelikesnake
{
    public class SoundManager : MonoBehaviour
    {
        public static SoundManager current;

        public GameObject AudioSource;

        public string ambiant1 = "Ambiant";
        public float ambiantVolume = 0.2f;
        public bool isAmbiantEnabled = false;

        public List<string> music1 = new List<string>();
        public float musicVolume = 0.2f;
        public bool isMusicEnabled = false;

        public List<string> tileAppear1 = new List<string>();
        public float tileAppearVolume = 0.02f;
        public bool isTileAppearEnabled = false;


        public List<string> tileDisappear1 = new List<string>();
        public float tileDisappearVolume = 0.05f;
        public bool isTileDisappearEnabled = false;


        public string doorUnlocked1 = "Door";
        public float doorUnlockedVolume = 0.3f;
        public bool isDoorUnlockedEnabled = false;

        public List<string> skelettonKilled1 = new List<string>();
        public float skelettonKilledVolume = 0.3f;
        public bool isSkelettonKilledEnabled = false;


        public List<string> potionPicked1 = new List<string>();
        public float potionPickedVolume = 0.7f;
        public bool isPotionPickedEnabled = false;

        public string piecePicked1 = "PickUpPiece";
        public float piecePickedVolume = 0.15f;
        public bool isPiecePickedEnabled = false;

        public string endGameGate1 = "GateEndLevel";
        public float endGameGateVolume = 0.2f;
        public bool isEndGameGateEnabled = false;

        public string screamFall1 = "ScreamFall";
        public float screamFallVolume = 0.4f;
        public bool isScreamFallEnabled = false;

        public string uiClick1 = "UiClick";
        public float uiClickVolume = 0.2f;
        public bool isUiClickEnabled = false;

        public string uiClick11 = "UiClick2";
        public float uiClick11Volume = 0.2f;
        public bool isUiClick11Enabled = false;

        public string musiqueMenu1 = "musiqueMenu";
        public float musiqueMenuVolume = 0.2f;
        public bool isMusiqueMenuEnabled = false;

        public string marchandHello1 = "MarchandHello";
        public float marchandHelloVolume = 0.2f;
        public bool isMarchandHello1Enabled = false;

        public string marchandGoodBye1 = "MarchandGoodbye";
        public float marchandGoodByeVolume = 0.2f;
        public bool isMarchandGoodByeEnabled = false;

        public AudioSource actualMusic;

        [Header("Sound Volume")]
        public float playerSoundVolume;
        public float playerMusicVolume;

        public void Awake()
        {
            current = this;
            ambiantSound();
            playerSoundVolume = PlayerPrefs.GetFloat("soundMult");
            playerMusicVolume = PlayerPrefs.GetFloat("musicMult");
        }


        public void musicMenu()
        {
            if (isMusiqueMenuEnabled)
            {
                GameObject obj = Instantiate(AudioSource);
                AudioSource audioSource = obj.GetComponent<AudioSource>();
                audioSource.clip = Resources.Load(musiqueMenu1, typeof(AudioClip)) as AudioClip;
                audioSource.volume = 0f;
                audioSource.loop = true;
                obj.GetComponent<DestroyAfterTime>().enabled = false;
                FadeIn fadein = obj.GetComponent<FadeIn>();
                fadein.isFadeIn = true;
                fadein.timeOfLerpSoundDownBeforeTargetVolume = 3f;
                fadein.targetVolume = musiqueMenuVolume * playerMusicVolume;
                fadein.enabled = true;
                actualMusic = audioSource;
                audioSource.Play();
            }
        }

        public void music()
        {
            //Set a random music
            //Set a random %time in the music
            if (isMusicEnabled)
            {
                if (actualMusic)
                    stopMusic();

                GameObject obj = Instantiate(AudioSource);
                AudioSource audioSource = obj.GetComponent<AudioSource>();
                audioSource.clip = Resources.Load(music1[Random.Range(0, music1.Count)], typeof(AudioClip)) as AudioClip;
                audioSource.volume = 0f;
                audioSource.loop = true;
                audioSource.time = Random.value * audioSource.clip.length;
                obj.GetComponent<DestroyAfterTime>().enabled = false;
                FadeIn fadein = obj.GetComponent<FadeIn>();
                fadein.isFadeIn = true;
                fadein.timeOfLerpSoundDownBeforeTargetVolume = 5f;
                fadein.targetVolume = musicVolume * playerMusicVolume;
                fadein.enabled = true;
                actualMusic = audioSource;
                audioSource.Play();
            }
        }

        public void stopMusic()
        {
            if (actualMusic)
            {
                Destroy(actualMusic.GetComponent<FadeIn>());
                actualMusic.GetComponent<DestroyAfterTime>().secondBeforeInitiatingDestroy = 0f;
                actualMusic.GetComponent<DestroyAfterTime>().timeOfLerpSoundDownBeforeDestroy = 1f;
                actualMusic.GetComponent<DestroyAfterTime>().enabled = true;
                actualMusic = null;
            }
        }

        public void ambiantSound()
        {
            if (isAmbiantEnabled)
            {
                GameObject obj = Instantiate(AudioSource);
                AudioSource audioSource = obj.GetComponent<AudioSource>();
                audioSource.clip = Resources.Load(ambiant1, typeof(AudioClip)) as AudioClip;
                audioSource.volume = ambiantVolume;
                audioSource.loop = true;
                obj.GetComponent<DestroyAfterTime>().enabled = false;
                audioSource.Play();
            }
        }

        public void tileAppearMassive()
        {

        }

        public void tileAppear()
        {
            if (isTileAppearEnabled)
            {
                GameObject obj = Instantiate(AudioSource);
                AudioSource audioSource = obj.GetComponent<AudioSource>();
                audioSource.clip = Resources.Load(tileAppear1[Random.Range(0, tileAppear1.Count)], typeof(AudioClip)) as AudioClip;
                audioSource.volume = tileAppearVolume * playerSoundVolume;
                audioSource.loop = false;
                obj.GetComponent<DestroyAfterTime>().secondBeforeInitiatingDestroy = 3f;
                obj.GetComponent<DestroyAfterTime>().enabled = true;
                audioSource.Play();
            }
        }

        public void tileDisappear()
        {
            if (isTileDisappearEnabled)
            {
                GameObject obj = Instantiate(AudioSource);
                AudioSource audioSource = obj.GetComponent<AudioSource>();
                audioSource.clip = Resources.Load(tileDisappear1[Random.Range(0, tileDisappear1.Count)], typeof(AudioClip)) as AudioClip;
                audioSource.volume = tileDisappearVolume * playerSoundVolume;
                audioSource.loop = false;
                obj.GetComponent<DestroyAfterTime>().secondBeforeInitiatingDestroy = 4f;
                obj.GetComponent<DestroyAfterTime>().enabled = true;
                audioSource.Play();
            }
        }

        public void doorUnlocked()
        {
            if(isDoorUnlockedEnabled)
            {
                GameObject obj = Instantiate(AudioSource);
                AudioSource audioSource = obj.GetComponent<AudioSource>();
                audioSource.clip = Resources.Load(doorUnlocked1, typeof(AudioClip)) as AudioClip;
                audioSource.volume = doorUnlockedVolume * playerSoundVolume;
                audioSource.loop = false;
                audioSource.time = 0.4f;
                obj.GetComponent<DestroyAfterTime>().enabled = true;
                audioSource.Play();
            }
        }

        public void potionPicked()
        {
            if (isPotionPickedEnabled)
            {
                GameObject obj = Instantiate(AudioSource);
                AudioSource audioSource = obj.GetComponent<AudioSource>();
                audioSource.clip = Resources.Load(potionPicked1[Random.Range(0, potionPicked1.Count)], typeof(AudioClip)) as AudioClip;
                audioSource.volume = potionPickedVolume * playerSoundVolume;
                audioSource.loop = false;
                obj.GetComponent<DestroyAfterTime>().secondBeforeInitiatingDestroy = 2f;
                obj.GetComponent<DestroyAfterTime>().enabled = true;
                audioSource.Play();
            }
        }

        public void piecePicked()
        {
            if (isPiecePickedEnabled)
            {
                GameObject obj = Instantiate(AudioSource);
                AudioSource audioSource = obj.GetComponent<AudioSource>();
                audioSource.clip = Resources.Load(piecePicked1, typeof(AudioClip)) as AudioClip;
                audioSource.volume = piecePickedVolume * playerSoundVolume;
                audioSource.loop = false;
                obj.GetComponent<DestroyAfterTime>().secondBeforeInitiatingDestroy = 2f;
                obj.GetComponent<DestroyAfterTime>().enabled = true;
                audioSource.Play();
            }
        }

        public void skelettonKilled()
        {
            if (isSkelettonKilledEnabled)
            {
                GameObject obj = Instantiate(AudioSource);
                AudioSource audioSource = obj.GetComponent<AudioSource>();
                audioSource.clip = Resources.Load(skelettonKilled1[Random.Range(0, skelettonKilled1.Count)], typeof(AudioClip)) as AudioClip;
                audioSource.volume = skelettonKilledVolume * playerSoundVolume;
                audioSource.loop = false;
                audioSource.time = 0.4f;
                obj.GetComponent<DestroyAfterTime>().enabled = true;
                audioSource.Play();
            }
        }

        public void endGameGateTravel()
        {
            if (isEndGameGateEnabled)
            {
                GameObject obj = Instantiate(AudioSource);
                AudioSource audioSource = obj.GetComponent<AudioSource>();
                audioSource.clip = Resources.Load(endGameGate1, typeof(AudioClip)) as AudioClip;
                audioSource.volume = endGameGateVolume * playerSoundVolume;
                audioSource.loop = false;
                obj.GetComponent<DestroyAfterTime>().secondBeforeInitiatingDestroy = 3f;
                obj.GetComponent<DestroyAfterTime>().enabled = true;
                audioSource.Play();
            }
        }

        public void screamFall()
        {
            if (isScreamFallEnabled)
            {
                GameObject obj = Instantiate(AudioSource);
                AudioSource audioSource = obj.GetComponent<AudioSource>();
                audioSource.clip = Resources.Load(screamFall1, typeof(AudioClip)) as AudioClip;
                audioSource.volume = screamFallVolume * playerSoundVolume;
                audioSource.loop = false;
                obj.GetComponent<DestroyAfterTime>().secondBeforeInitiatingDestroy = 5f;
                obj.GetComponent<DestroyAfterTime>().enabled = true;
                audioSource.Play();
            }
        }

        public void uiClick()
        {
            if (isUiClickEnabled)
            {
                GameObject obj = Instantiate(AudioSource);
                AudioSource audioSource = obj.GetComponent<AudioSource>();
                audioSource.clip = Resources.Load(uiClick1, typeof(AudioClip)) as AudioClip;
                audioSource.volume = uiClickVolume * playerSoundVolume;
                audioSource.loop = false;
                obj.GetComponent<DestroyAfterTime>().secondBeforeInitiatingDestroy = 3f;
                obj.GetComponent<DestroyAfterTime>().enabled = true;
                audioSource.Play();
            }
        }

        public void uiClick2()
        {
            if (isUiClick11Enabled)
            {
                GameObject obj = Instantiate(AudioSource);
                AudioSource audioSource = obj.GetComponent<AudioSource>();
                audioSource.clip = Resources.Load(uiClick11, typeof(AudioClip)) as AudioClip;
                audioSource.volume = uiClick11Volume * playerSoundVolume;
                audioSource.loop = false;
                obj.GetComponent<DestroyAfterTime>().secondBeforeInitiatingDestroy = 3f;
                obj.GetComponent<DestroyAfterTime>().enabled = true;
                audioSource.Play();
            }
        }

        public void marchandHello()
        {
            if (isMarchandHello1Enabled)
            {
                GameObject obj = Instantiate(AudioSource);
                AudioSource audioSource = obj.GetComponent<AudioSource>();
                audioSource.clip = Resources.Load(marchandHello1, typeof(AudioClip)) as AudioClip;
                audioSource.volume = marchandHelloVolume * playerSoundVolume;
                audioSource.loop = false;
                obj.GetComponent<DestroyAfterTime>().secondBeforeInitiatingDestroy = 5f;
                obj.GetComponent<DestroyAfterTime>().enabled = true;
                audioSource.Play();
            }
        }

        public void marchandGoodbye()
        {
            if (isMarchandGoodByeEnabled)
            {
                GameObject obj = Instantiate(AudioSource);
                AudioSource audioSource = obj.GetComponent<AudioSource>();
                audioSource.clip = Resources.Load(marchandGoodBye1, typeof(AudioClip)) as AudioClip;
                audioSource.volume = marchandGoodByeVolume * playerSoundVolume;
                audioSource.loop = false;
                obj.GetComponent<DestroyAfterTime>().secondBeforeInitiatingDestroy = 5f;
                obj.GetComponent<DestroyAfterTime>().enabled = true;
                audioSource.Play();
            }
        }
    }
}
