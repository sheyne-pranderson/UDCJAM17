﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace roguelikesnake
{
    public class FadeIn : MonoBehaviour
    {
        public float timeOfLerpSoundDownBeforeTargetVolume = 0.3f;
        public float targetVolume = 0.1f;
        public bool isFadeIn = false;
        public AudioSource audioClip;

        private void OnEnable()
        {
            if(isFadeIn)
                StartCoroutine(InitiateFadeIn());
        }

        public IEnumerator InitiateFadeIn()
        {
            float elapsedTime = 0f;


            while (elapsedTime <= timeOfLerpSoundDownBeforeTargetVolume)
            {
                elapsedTime += Time.deltaTime;
                audioClip.volume = Mathf.Lerp(0, targetVolume, (elapsedTime / timeOfLerpSoundDownBeforeTargetVolume));

                yield return null;
            }
            audioClip.volume = targetVolume;
        }
    }
}
