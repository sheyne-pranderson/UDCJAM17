﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace roguelikesnake
{
    public class DestroyAfterTime : MonoBehaviour
    {
        public float secondBeforeInitiatingDestroy = 2f;
        public float timeOfLerpSoundDownBeforeDestroy = 0.3f;
        public AudioSource audioClip;

        private void OnEnable()
        {
            StartCoroutine(InitiateDestroy());
        }

        public IEnumerator InitiateDestroy()
        {
            yield return new WaitForSeconds(secondBeforeInitiatingDestroy);

            float elapsedTime = 0f;

            while (elapsedTime <= timeOfLerpSoundDownBeforeDestroy)
            {
                elapsedTime += Time.deltaTime;
                audioClip.volume = Mathf.Lerp(audioClip.volume, 0, (elapsedTime / timeOfLerpSoundDownBeforeDestroy));

                yield return null;
            }

            Destroy(gameObject);

        }
    }
}
