﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace roguelikesnake
{
	public class Room : MonoBehaviour
	{
		public Corridor CorridorPrefab;
		public IntVector2 Size;
		public IntVector2 Coordinates;
		public int Num;

		private GameObject _tilesObject;
		private GameObject _wallsObject;
		private GameObject _monstersObject;
		public Tile TilePrefab;
		private Tile[,] _tiles;
		public GameObject WallPrefab;
		public RoomSetting Setting;

		public Dictionary<Room, Corridor> RoomCorridor = new Dictionary<Room, Corridor>();

		private Map _map;

		private GameManager gm;
		private RoomMapManager rmm;

		public List<Tile> roomTiles = new List<Tile>();

		public GameObject PlayerPrefab;
		//public GameObject MonsterPrefab;
		//public int MonsterCount;
		//private GameObject[] Monsters;

		public void Awake()
		{
			gm = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
			rmm = GameObject.FindGameObjectWithTag("GameController").GetComponent<RoomMapManager>();
		}

		public void Init(Map map)
		{
			_map = map;
		}

		public IEnumerator Generate()
		{
			//todo

			// Create parent object
			_tilesObject = new GameObject("Tiles");
			_tilesObject.transform.parent = transform;
			_tilesObject.transform.localPosition = Vector3.zero;

			_tiles = new Tile[Size.x, Size.z];
			for (int x = 0; x < Size.x; x++)
			{
				for (int z = 0; z < Size.z; z++)
				{
					_tiles[x, z] = CreateTile(new IntVector2((Coordinates.x + x), Coordinates.z + z));
				}
			}
			yield return null;
		}

		private Tile CreateTile(IntVector2 coordinates)
		{
			if (_map.GetTileType(coordinates) == TileType.Empty)
			{
				_map.SetTileType(coordinates, TileType.Room);
			}
			else
			{
				Debug.LogError("Tile Conflict!");
			}
			Tile newTile = Instantiate(TilePrefab);
			//Debug.Log("Room.cs = " +gm.randomColors[0] +" " + gm.randomColors[1]);
			newTile.GetComponentInChildren<Renderer>().material.SetColor("_TopColor", gm.randomColors[0]);
			newTile.GetComponentInChildren<Renderer>().material.SetColor("_BotColor", gm.randomColors[1]);
			newTile.Coordinates = coordinates;
			newTile.name = "Tile " + coordinates.x + ", " + coordinates.z;
			newTile.transform.parent = _tilesObject.transform;
			newTile.transform.localPosition = RoomMapManager.TileSize * new Vector3(coordinates.x - Coordinates.x - Size.x * 0.5f + 0.5f, 0f, coordinates.z - Coordinates.z - Size.z * 0.5f + 0.5f);

			float randomHeight = Random.Range(-0.1f, 0.1f);

			Transform floorChild = newTile.transform.GetChild(0).transform;
			int yHeight = Random.Range(rmm.floorMinHeight, rmm.floorMaxHeight);
			floorChild.transform.localScale = new Vector3(1, yHeight, 1);
			floorChild.transform.localPosition = new Vector3(0, -((yHeight / 2f) + randomHeight - 0.5f), 0);


			rmm.AllRoomsTiles.Add(newTile);
			roomTiles.Add(newTile);
			//if(newTile.transform.childCount > 0)
			//	newTile.transform.GetChild(0).GetComponent<Renderer>().material = Setting.floor;
			return newTile;
		}

		public Corridor CreateCorridor(Room otherRoom)
		{
			// Don't create if already connected
			if (RoomCorridor.ContainsKey(otherRoom))
			{
				return RoomCorridor[otherRoom];
			}

			Corridor newCorridor = Instantiate(CorridorPrefab);
			newCorridor.name = "Corridor (" + otherRoom.Num + ", " + Num + ")";
			newCorridor.transform.parent = transform.parent;
			newCorridor.Coordinates = new IntVector2(Coordinates.x + Size.x / 2, otherRoom.Coordinates.z + otherRoom.Size.z / 2);
			newCorridor.transform.localPosition = new Vector3(newCorridor.Coordinates.x - _map.MapSize.x / 2, 0, newCorridor.Coordinates.z - _map.MapSize.z / 2);
			newCorridor.Rooms[0] = otherRoom;
			newCorridor.Rooms[1] = this;
			newCorridor.Length = Vector3.Distance(otherRoom.transform.localPosition, transform.localPosition);
			newCorridor.Init(_map);
			otherRoom.RoomCorridor.Add(this, newCorridor);
			RoomCorridor.Add(otherRoom, newCorridor);

			return newCorridor;
		}

		public IEnumerator CreateWalls()
		{
			_wallsObject = new GameObject("Walls");
			_wallsObject.transform.parent = transform;
			_wallsObject.transform.localPosition = Vector3.zero;

			IntVector2 leftBottom = new IntVector2(Coordinates.x - 1, Coordinates.z - 1);
			IntVector2 rightTop = new IntVector2(Coordinates.x + Size.x, Coordinates.z + Size.z);
			for (int x = leftBottom.x; x <= rightTop.x; x++)
			{
				for (int z = leftBottom.z; z <= rightTop.z; z++)
				{
					// If it's center or corner or not wall
					if ((x != leftBottom.x && x != rightTop.x && z != leftBottom.z && z != rightTop.z) ||
						((x == leftBottom.x || x == rightTop.x) && (z == leftBottom.z || z == rightTop.z)) ||
						(_map.GetTileType(new IntVector2(x, z)) != TileType.Wall))
					{
						continue;
					}
					Quaternion rotation = Quaternion.identity;
					if (x == leftBottom.x)
					{
						rotation = MapDirection.West.ToRotation();
					}
					else if (x == rightTop.x)
					{
						rotation = MapDirection.East.ToRotation();
					}
					else if (z == leftBottom.z)
					{
						rotation = MapDirection.South.ToRotation();
					}
					else if (z == rightTop.z)
					{
						rotation = MapDirection.North.ToRotation();
					}
					else
					{
						Debug.LogError("Wall is not on appropriate location!!");
					}

					GameObject newWall = Instantiate(WallPrefab);
					newWall.name = "Wall (" + x + ", " + z + ")";
					newWall.transform.parent = _wallsObject.transform;
					newWall.transform.localPosition = RoomMapManager.TileSize * new Vector3(x - Coordinates.x - Size.x * 0.5f + 0.5f, 0f, z - Coordinates.z - Size.z * 0.5f + 0.5f);
					newWall.transform.localRotation = rotation;
					newWall.transform.localScale *= RoomMapManager.TileSize;
					newWall.transform.GetChild(0).GetComponent<Renderer>().material = Setting.wall;
				}
			}
			yield return null;
		}

		/*	public IEnumerator CreateMonsters()
			{
				_monstersObject = new GameObject("Monsters");
				_monstersObject.transform.parent = transform;
				_monstersObject.transform.localPosition = Vector3.zero;

				Monsters = new GameObject[MonsterCount];

				for (int i = 0; i < MonsterCount; i++)
				{
					GameObject newMonster = Instantiate(MonsterPrefab);
					newMonster.name = "Monster " + (i + 1);
					newMonster.transform.parent = _monstersObject.transform;
					newMonster.transform.localPosition = new Vector3(i / 2f, 0f, i % 2f);
					Monsters[i] = newMonster;
				}
				yield return null;
			}*/

		public IEnumerator CreatePlayer()
		{
			GameObject player = Instantiate((PlayerPrefab));
			player.name = "Player";
			//player.transform.parent = transform.parent;
			player.transform.localPosition = new Vector3(transform.localPosition.x, 1, transform.localPosition.z);

			yield return null;
		}

		public Color[] randomColors()
		{
			float H, S, V;
			Color top = Random.ColorHSV(0f, 1f, 0.6f, 0.7f, 0.3f, 0.4f);
			Color.RGBToHSV(top, out H, out S, out V);
			Color bot = Color.HSVToRGB(H + 0.15f, S, 1);
			Color[] valRet = { top, bot };
			Debug.Log(top + " " + bot);
			return valRet;
		}
	}
}