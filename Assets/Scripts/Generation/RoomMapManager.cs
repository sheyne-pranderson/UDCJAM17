﻿using UnityEngine;
using UnityEngine.InputSystem;
using System;
using System.Collections.Generic;

namespace roguelikesnake
{
	public class RoomMapManager : MonoBehaviour
	{
		public Map mapPrefap;
		public Map mapInstance;
		public Controls controls;

		public int MapSizeX;
		public int MapSizeZ;
		public int MaxRooms;
		public int MinRoomSize;
		public int MaxRoomSize;

		public int floorMinHeight = 3;
		public int floorMaxHeight = 8;

		public static int TileSize = 1;


		public List<Tile> AllRoomsTiles = new List<Tile>();
		public List<Tile> AllCorridorTiles = new List<Tile>();

		private void Start()
		{
			BeginGame();
		}

		private void Awake()
		{
			controls = new Controls();
		}

		private void OnEnable()
		{
			controls.PlayerGame.Restart.performed += RestartGame;
			controls.PlayerGame.Restart.Enable();
		}

		private void OnDisable()
		{
			controls.PlayerGame.Restart.performed -= RestartGame;
			controls.PlayerGame.Restart.Disable();
		}

		private void BeginGame()
		{
			mapInstance = Instantiate(mapPrefap, transform);
			mapInstance.RoomCount = MaxRooms;
			mapInstance.MapSize = new IntVector2(MapSizeX, MapSizeZ);
			mapInstance.RoomSize.Min = MinRoomSize;
			mapInstance.RoomSize.Max = MaxRoomSize;

			StartCoroutine(mapInstance.Generate());
		}

		public void RestartGameProg()
		{
			StopAllCoroutines();
			AllRoomsTiles = new List<Tile>();
			AllCorridorTiles = new List<Tile>();
			Destroy(mapInstance.gameObject);
			BeginGame();
		}

		private void RestartGame(InputAction.CallbackContext context)
		{
			gameObject.GetComponent<GameManager>().restartGame();
		}

		public bool isCorridorTile(Tile tile)
		{
			return AllCorridorTiles.Contains(tile);
		}

		public List<Tile> getAllTilesConnectedToRooms()
        {
			List<Tile> tilesConnectedToRooms = new List<Tile>();

			foreach(Tile tile in AllCorridorTiles)
            {
				List<Tile> aroundTiles = tile.getAroundTiles();
				if (aroundTiles.Count > 0)
                {
					foreach(Tile aroundTile in aroundTiles)
                    {
						if(aroundTile.CompareTag("FloorTile"))
                        {
							if (!tilesConnectedToRooms.Contains(tile))
							{
								tile.ifCorridorTileThisIsTheRoomTileConnectedTo = aroundTile;
								aroundTile.ifRoomTileThisIsTheCorridorTileConnectedTo = tile;
								tilesConnectedToRooms.Add(tile);
							}
						}
                    }
                }
            }

			return tilesConnectedToRooms;
        }

		public Corridor getCorridorByTile(Tile tile)
		{
			foreach (Corridor corridor in mapInstance._corridors)
			{
				foreach (Tile corridorTile in corridor.corridorTiles)
				{
					if (corridorTile == tile)
					{
						return corridor;
					}
				}
			}

			return default(Corridor);
		}

		public Room getRoomByTile(Tile tile)
        {
			foreach(Room room in mapInstance._rooms)
            {
				foreach(Tile roomTile in room.roomTiles)
                {
					if(roomTile == tile)
                    {
						return room;
                    }
                }
            }

			return default(Room);
        }
	}
}