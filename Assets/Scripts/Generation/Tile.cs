﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace roguelikesnake
{
	public class Tile : MonoBehaviour
	{
		public IntVector2 Coordinates;

		public Vector3 basePosition = new Vector3();
		public Quaternion baseRotation = new Quaternion();
		public Vector3 baseLocalscale = new Vector3();
		public bool mustReappear = false;
		public bool isVisible = false;


		//Used to instanciate the door with the rotation of tile - roomTile
		public Tile ifCorridorTileThisIsTheRoomTileConnectedTo = null;
		public Tile ifRoomTileThisIsTheCorridorTileConnectedTo = null;

		public void Awake()
		{
			//StartCoroutine(animateAppearing());
			setInvisible();
		}

		public void setVisible()
        {
			MeshRenderer mr = transform.GetChild(0).GetComponent<MeshRenderer>();
			mr.enabled = true;
			isVisible = true;
		}

		public void setInvisible()
		{
			MeshRenderer mr = transform.GetChild(0).GetComponent<MeshRenderer>();
			mr.enabled = false;
			isVisible = false;
		}

		public IEnumerator animateAppearing()
		{
			if (transform.childCount > 0)
			{
				Transform child = transform.GetChild(0);
				Vector3 baseChildLocalscale = child.localScale;
				child.localScale = new Vector3(0, 0, 0);
				//Animate the tile reapearition
				float scaleX = 0f;
				float scaleZ = 0f;
				float scaleY = 0f;
				float elapsedTime = 0f;
				float waitTime = 4f; //TODO MAKE THIS A GAMEMANAGER VALUE

				while (elapsedTime < waitTime)
				{
					scaleX = Mathf.Lerp(child.localScale.x, baseChildLocalscale.x, (elapsedTime / waitTime));
					scaleZ = Mathf.Lerp(child.localScale.z, baseChildLocalscale.z, (elapsedTime / waitTime));
					scaleY = Mathf.Lerp(child.localScale.y, baseChildLocalscale.y, (elapsedTime / waitTime / 2));
					elapsedTime += Time.deltaTime;

					child.localScale = new Vector3(scaleX, scaleY, scaleZ);
					// Yield here
					yield return null;
				}

				// Make sure we got there
				child.localScale = baseChildLocalscale;
			}
			yield return null;
		}

		public IEnumerator onPlayerExited(CharacterController characterController)
		{
			basePosition = transform.position;
			baseRotation = transform.rotation;
			baseLocalscale = transform.localScale;

			//Drop the tile.
			//Simulate gravity
			float gravityDropTime = 3f; //TODO MAKE THIS A GAMEMANAGER VALUE
			float elapsedTime = 0f;
			float gravityDropSpeed = 0.04f;
			float acceleration = 0f;
			float soundAfter = 0f;
			bool soundPlayed = false;
			SoundManager.current.tileDisappear();
			while (elapsedTime < gravityDropTime)
			{

				elapsedTime += Time.deltaTime;
				acceleration = acceleration + gravityDropSpeed * 9.81f * Time.deltaTime;
				transform.position = new Vector3(transform.position.x, transform.position.y - (acceleration), transform.position.z);

				if (mustReappear)
					elapsedTime = gravityDropTime;

				yield return null;
			}

			yield return null;
		}

		public IEnumerator reappear(CharacterController characterController)
		{
			//Remove the tile from the characterController tile array
			characterController.oldTiles.Remove(this.gameObject);
			mustReappear = true;
			//Make sure to exit the coroutine onPlayerExited
			yield return new WaitForEndOfFrame();
			mustReappear = false;

			yield return new WaitForEndOfFrame();

			Transform child = transform.GetChild(0);
			Vector3 baseChildLocalscale = child.localScale;

			//Appear the tile back
			transform.position = basePosition;
			transform.rotation = baseRotation;
			child.transform.localScale = new Vector3(0, baseChildLocalscale.y, 0);

			yield return new WaitForEndOfFrame();

			SoundManager.current.tileAppear();

			//Animate the tile reapearition
			float scaleX = 0f;
			float scaleZ = 0f;
			float elapsedTime = 0f;
			float waitTime = 1f; //TODO MAKE THIS A GAMEMANAGER VALUE

			while (elapsedTime < waitTime)
			{
				scaleX = Mathf.Lerp(child.localScale.x, baseChildLocalscale.x, (elapsedTime / waitTime));
				scaleZ = Mathf.Lerp(child.localScale.z, baseChildLocalscale.z, (elapsedTime / waitTime));
				elapsedTime += Time.deltaTime;

				child.localScale = new Vector3(scaleX, baseChildLocalscale.y, scaleZ);

				// Yield here
				yield return null;
			}

			// Make sure we got there
			child.localScale = baseChildLocalscale;
			yield return null;

		}

		public List<Tile> getAroundTiles()
        {
			List<Tile> tiles = new List<Tile>();
			RaycastHit hit;
			// Does the ray intersect any objects excluding the player layer
			if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.left), out hit, 0.8f))
			{
				Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.left) * hit.distance, Color.yellow);
				if (hit.collider.CompareTag("FloorTile") || hit.collider.CompareTag("connexion"))
				{
					tiles.Add(hit.collider.GetComponent<Tile>());
				}
			}
			if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), out hit, 0.8f))
			{
				Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.right) * hit.distance, Color.yellow);
				if (hit.collider.CompareTag("FloorTile") || hit.collider.CompareTag("connexion"))
				{
					tiles.Add(hit.collider.GetComponent<Tile>());
				}
			}
			if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 0.8f))
			{
				Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
				if (hit.collider.CompareTag("FloorTile") || hit.collider.CompareTag("connexion"))
				{
					tiles.Add(hit.collider.GetComponent<Tile>());
				}
			}
			if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.back), out hit, 0.8f))
			{
				Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.back) * hit.distance, Color.yellow);
				if (hit.collider.CompareTag("FloorTile") || hit.collider.CompareTag("connexion"))
				{
					tiles.Add(hit.collider.GetComponent<Tile>());
				}
			}
			return tiles;
		}

	}
}
