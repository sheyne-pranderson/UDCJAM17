﻿using UnityEngine;
using System;

namespace roguelikesnake
{
	[Serializable]
	public class RoomSetting
	{
		public Material floor, wall;
	}
}