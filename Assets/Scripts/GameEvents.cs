﻿using System;
using UnityEngine;

namespace roguelikesnake
{
	public class GameEvents : MonoBehaviour
	{
		public static GameEvents current;

		public void Awake()
		{
			current = this;
		}

		public event Action<Marchand> OnClosedShop;

		public void ClosedShop(Marchand thisDamnGuy)
		{
			if (OnClosedShop != null)
			{
				OnClosedShop(thisDamnGuy);
			}
		}

		public event Action OnPlayerCollideWallOrVoid;

		public void PlayerCollideWallOrVoid()
		{
			SoundManager.current.stopMusic();
			if (OnPlayerCollideWallOrVoid != null)
			{
				OnPlayerCollideWallOrVoid();
			}
		}

		public event Action OnPlayerWin;

		public void PlayerWin()
		{
			if (OnPlayerWin != null)
			{
				OnPlayerWin();
			}
		}

		public event Action OnLevelFullyReady;

		public void LevelFullyReady()
		{
			SoundManager.current.music();
			if (OnLevelFullyReady != null)
			{
				OnLevelFullyReady();
			}
		}

		public event Action OnUpgradingUI;

		public void UpgradingUI()
		{
			if (OnUpgradingUI != null)
			{
				OnUpgradingUI();
			}
		}
	}
}