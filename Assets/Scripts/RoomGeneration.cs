﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomGeneration : MonoBehaviour
{
    public int width = 5;
    public int height = 5;
    /*public*/ int groundHeight = 5;
    public int groundMinHeight = 3;
    public int groundMaxHeight = 8;

    public GameObject floorPrefab;

    private void CreateBasicCube(int x, int z, int yHeight)
    {
        GameObject boxColl = new GameObject();
        boxColl.transform.rotation = this.transform.rotation;
        boxColl.transform.parent = this.transform;
        boxColl.transform.localPosition = new Vector3(x, 0, z);
        boxColl.name = transform.name + "_" + x + "_" + z + "_coll";
        boxColl.tag = "FloorTile";
        boxColl.AddComponent<BoxCollider>();
        BoxCollider bc = boxColl.GetComponent<BoxCollider>();
        //bc.center = boxColl.transform.position;

        GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Cube);
        obj.transform.rotation = this.transform.rotation;
        obj.GetComponent<Renderer>().sharedMaterial = this.GetComponent<Renderer>().sharedMaterial;
        obj.transform.parent = boxColl.transform;
        DestroyImmediate(obj.GetComponent<BoxCollider>());
        //obj.tag = "FloorTile";
        obj.transform.localScale = new Vector3(1, yHeight, 1);
        obj.transform.localPosition = new Vector3(0, -((yHeight/2f) - 0.5f), 0);
        obj.name = transform.name + "_" + x + "_" + z;
    }

    public void GenerateRoom()
	{
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                GameObject floor = Instantiate(floorPrefab, new Vector3(transform.position.x + x, 0, transform.position.y + y), transform.rotation, transform);
                floor.name = transform.name + "_" + x + "_" + y;
                Transform floorChild = floor.transform.GetChild(0).transform;
                float randomHeight = Random.Range(-0.1f, 0.1f);
                int yHeight = Random.Range(groundMinHeight, groundMaxHeight);
                floorChild.transform.localScale = new Vector3(1, yHeight, 1);
                floorChild.transform.localPosition = new Vector3(0, -((yHeight / 2f) + randomHeight - 0.5f), 0);
            }
        }
    }

    public void DeleteAllGameObjectChildren()
    {
        Debug.Log(transform.childCount + " gameObject deleted!");
        while (transform.childCount != 0)
        {
            foreach (Transform child in transform)
            {
                GameObject.DestroyImmediate(child.gameObject);
            }
        }
    }

    public void GenerateRoomFromPrimitive()
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                int rh = Random.Range(groundMinHeight, groundMaxHeight);
                //int rh = groundHeight;
                CreateBasicCube(x, y, rh);
            }
        }
    }
}
