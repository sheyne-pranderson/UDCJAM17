﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonGeneration : MonoBehaviour
{
    [Header("Dungeon setup")]
    public int nbrRoom;
    public List<GameObject> startingRooms;
    public List<GameObject> normalRooms;
    public List<GameObject> hallways;
    [Header("Dungeon")]
    public List<GameObject> actualDungeonRooms;

    // Start is called before the first frame update
    void Start()
    {
        //Spawn starting room
        GameObject startRoom = SpawnStartingRoom();
        DungeonConnexion[] startConnexions = startRoom.GetComponentsInChildren<DungeonConnexion>();

        //Debug.Log(startConnexions.Length != 0);
        if (startConnexions.Length != 0)
		{
            int sc = Random.Range(0, startConnexions.Length);
            startConnexions[sc].SpawnHallway();

            foreach (var item in startConnexions)
            {
                //Debug.Log(item);
            }
        }

        
        DungeonCreation();
        //Get the first available room that has any unconnected openings
        //Tell the room to spawn adjacent chambers
        //Wait until the room's opening have spawned
        //Add Created Rooms to the dungeon
        //repeat 2-6 until the max number of room is generated

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void DungeonCreation()
    {
        Debug.Log(actualDungeonRooms.Count + "/" + nbrRoom + " " + (actualDungeonRooms.Count < nbrRoom));
        while (actualDungeonRooms.Count < nbrRoom)
        {
            SpawnRoom();
            Debug.Log(actualDungeonRooms.Count + "/" + nbrRoom);

        }
    }

    private GameObject SpawnStartingRoom()
	{
		if (startingRooms.Count != 0)
		{
            int sr = Random.Range(0, startingRooms.Count);
            GameObject newRoom = Instantiate(startingRooms[sr], this.transform);
            actualDungeonRooms.Add(newRoom);
            return newRoom;
		}
		else
		{
            return null;
		}
	}

    private void SpawnRoom()
    {
        int r = Random.Range(0, normalRooms.Count);
        int test = Random.Range(0, 15);
        GameObject newRoom = Instantiate(normalRooms[r], new Vector3(test, 0, test), transform.rotation);
        DungeonPiece dp = newRoom.GetComponent<DungeonPiece>();
        actualDungeonRooms.Add(newRoom);
    }
}
