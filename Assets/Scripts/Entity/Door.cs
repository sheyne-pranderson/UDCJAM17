﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace roguelikesnake
{
	public class Door : GlobalEntity
	{
		GameManager gm;

		// Start is called before the first frame update
		void Start()
		{
			gm = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
		}

        private void Awake()
        {
			//StartCoroutine(animateAppearing());
			setInvisible();
		}

		public IEnumerator destroyTheDoor()
		{
			//Set the new material instance on the door
			MeshRenderer mr = transform.GetChild(0).GetComponent<MeshRenderer>();

			float originalTimeScale = Time.timeScale;
			Time.timeScale = originalTimeScale / 4;


			if (transform.childCount > 0)
			{

				Transform child = transform.GetChild(0);
				float dissolve = 4.25f;
				float elapsedTime = 0f;
				float waitTime = 3f;
				float timeBeforeSpeedBack = 0.3f;
				bool isTimeBack = false;

				SoundManager.current.doorUnlocked();

				while (elapsedTime < waitTime)
				{
					if(!isTimeBack && elapsedTime > timeBeforeSpeedBack)
                    {
						isTimeBack = true;
						CameraShake cameraShake = GameObject.FindGameObjectWithTag("MainCamera").transform.GetChild(0).GetComponent<CameraShake>();
						cameraShake.shakeDuration = 0.15f;
						cameraShake.shakeAmount = 0.9f;
						Time.timeScale = originalTimeScale;
					}

					dissolve = Mathf.Lerp(dissolve, 0, (elapsedTime / waitTime));
					elapsedTime += Time.unscaledDeltaTime;

					mr.materials[0].SetFloat("_CutoffHeight", dissolve);
					mr.materials[1].SetFloat("_CutoffHeight", dissolve);
					// Yield here
					yield return null;
				}
				Time.timeScale = originalTimeScale;
				// Make sure we got there
				mr.materials[0].SetFloat("_CutoffHeight", 0f);
				mr.materials[1].SetFloat("_CutoffHeight", 0f);
			}
			yield return null;
			Destroy(gameObject);
		}

		void OnTriggerEnter(Collider collider)
		{
			if (collider.transform.tag == "Player")
			{
				gm.breakADoor(this);
			}
		}
	}
}
