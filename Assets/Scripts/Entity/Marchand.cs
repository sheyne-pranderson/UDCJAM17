﻿using System.Collections;
using UnityEngine;
using UnityEngine.Rendering;


namespace roguelikesnake
{
	public class Marchand : GlobalEntity
	{
		private GameManager gm;
		private Transform renderer;

		public GameObject Eyes;
		public GameObject Lantern;
		public GameObject Pipe;

		// Start is called before the first frame update
		private void Start()
		{
			gm = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
			GameEvents.current.OnClosedShop += ShopIsClosed;
		}

		private void OnDisable()
		{
			GameEvents.current.OnClosedShop -= ShopIsClosed;
		}

		private void Awake()
		{
			renderer = transform.GetChild(0);
			//StartCoroutine(animateAppearing());
			setInvisible();
		}

		public void setInvisible()
        {
			renderer.gameObject.SetActive(false);
			isVisible = false;
		}

		public void setVisible()
		{
			renderer.gameObject.SetActive(true);
			isVisible = true;
		}

		private void OnTriggerEnter(Collider collider)
		{
			if (collider.transform.tag == "Player" && isVisible)
			{
				//GameEvents.current.OpenShop();
				gm.actualMarchand = this;
				gm.cc.isPause = true;
				gm.ShowShopUI();
			}
		}

		public void ShopIsClosed(Marchand dude)
		{
			if (dude == this)
			{
				StartCoroutine(closeMerchand());
			}
		}

		public IEnumerator closeMerchand()
		{

			if (transform.childCount > 0)
			{
				float dissolve = 4.25f;
				float elapsedTime = 0f;
				float waitTime = 3f;
				float timeBeforeSpeedBack = 0.3f;
				bool isTimeBack = false;

				//SoundManager.current.skelettonKilled();

				while (elapsedTime < waitTime)
				{
					if (!isTimeBack && elapsedTime > timeBeforeSpeedBack)
					{
						isTimeBack = true;
						Pipe.SetActive(false);
						Eyes.SetActive(false);
						Lantern.SetActive(false);
					}

					dissolve = Mathf.Lerp(dissolve, 0, (elapsedTime / waitTime));
					elapsedTime += Time.unscaledDeltaTime;

					setAllMaterialsValue("_CutoffHeight", dissolve, renderer);

					// Yield here
					yield return null;
				}

				// Make sure we got there
				setAllMaterialsValue("_CutoffHeight", 0f, renderer);

				yield return null;

				gameObject.SetActive(false);

			}
			yield return null;
		}

		private void Update()
        {
            if(gm.cc)
            {
				renderer.LookAt(new Vector3(gm.cc.transform.position.x, transform.position.y - 0.8f, gm.cc.transform.position.z));

			}
        }
    }
}