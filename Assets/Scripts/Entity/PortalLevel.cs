﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace roguelikesnake
{
    public class PortalLevel : MonoBehaviour
    {
        private GameManager gm;
		public GameObject child;

        // Start is called before the first frame update
        private void Start()
        {
            gm = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
			child.SetActive(false);
		}

        // Update is called once per frame
        void Update()
        {
			if (gm.actualLevelScore >= gm.maxScore && !child.activeInHierarchy)
			{
				StartCoroutine(animateAppearing());
				child.SetActive(true);
			}
        }

		public IEnumerator animateAppearing()
		{
			if (transform.childCount > 0)
			{
				Transform child = transform.GetChild(0);
				Vector3 baseChildLocalscale = child.localScale;
				child.localScale = new Vector3(0, 0, 0);
				//Animate the tile reapearition
				float scaleX = 0f;
				float scaleZ = 0f;
				float scaleY = 0f;
				float elapsedTime = 0f;
				float waitTime = 4f; //TODO MAKE THIS A GAMEMANAGER VALUE

				while (elapsedTime < waitTime)
				{
					scaleX = Mathf.Lerp(child.localScale.x, baseChildLocalscale.x, (elapsedTime / waitTime));
					scaleZ = Mathf.Lerp(child.localScale.z, baseChildLocalscale.z, (elapsedTime / waitTime));
					scaleY = Mathf.Lerp(child.localScale.y, baseChildLocalscale.y, (elapsedTime / waitTime / 2));
					elapsedTime += Time.deltaTime;

					child.localScale = new Vector3(scaleX, scaleY, scaleZ);
					// Yield here
					yield return null;
				}

				// Make sure we got there
				child.localScale = baseChildLocalscale;
			}
			yield return null;
		}
	}
}
