﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace roguelikesnake
{
	public class Potion : GlobalEntity
	{
		GameManager gm;

		Transform child;
		private Transform renderer;
		private float sin = 1f;
		private float sinHeigh = 3f;
		private float rotationSpeed = 1f;
		private float baseHeigh;
		public float startSin;

		// Start is called before the first frame update
		void Start()
		{
			startSin = Random.Range(-0.2f, 0.2f);
			sin += startSin;
			child = transform.GetChild(0);
			baseHeigh = child.transform.position.y;
			gm = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
		}
		private void Awake()
		{
			renderer = transform.GetChild(0);
			//StartCoroutine(animateAppearing());
			setInvisible();
		}
		public void setInvisible()
		{
			renderer.gameObject.SetActive(false);
			isVisible = false;
		}

		public void setVisible()
		{
			renderer.gameObject.SetActive(true);
			isVisible = true;
		}

		// Update is called once per frame
		void Update()
		{
			child.eulerAngles = new Vector3(child.eulerAngles.x, child.eulerAngles.y + rotationSpeed, child.eulerAngles.z);
			child.position = new Vector3(child.position.x, baseHeigh + Mathf.Sin(Time.time * sin) / sinHeigh, child.position.z);
		}

		void OnTriggerEnter(Collider collider)
		{
			if (collider.transform.tag == "Player")
			{
				if (gm.actualHp + 1 <= gm.maxHp)
				{
					gm.actualHp++;

				}
				gm.AddScore();
				SoundManager.current.potionPicked();

				if (!isLinkedToRoom)
				{
					manageRespawn();
				}
				else
				{
					gameObject.SetActive(false);
				}
			}
		}

		public void manageRespawn()
        {
			int p = Random.Range(0, gm.allTiles.Length);
			transform.position = gm.LocationOkToSpawn(new Vector3(gm.allTiles[p].transform.position.x, 1, gm.allTiles[p].transform.position.z));
			manageVisibility();
		}
	}
}
