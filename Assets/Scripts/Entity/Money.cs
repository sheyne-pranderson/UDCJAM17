﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace roguelikesnake
{
	public class Money : GlobalEntity
	{
		GameManager gm;
		Transform child;
		private float sin = 1f;
		private float sinHeigh = 3f;
		private float rotationSpeed = 1f;
		private float baseHeigh;
		// Start is called before the first frame update
		void Start()
		{
			child = transform.GetChild(0);
			baseHeigh = child.transform.position.y;
			gm = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
		}
		private void Awake()
		{
			//StartCoroutine(animateAppearing());
			setInvisible();
		}

		// Update is called once per frame
		void Update()
		{
			child.eulerAngles = new Vector3(child.eulerAngles.x, child.eulerAngles.y + rotationSpeed, child.eulerAngles.z);
			child.position = new Vector3(child.position.x, baseHeigh + Mathf.Sin(Time.time * sin) / sinHeigh, child.position.z);
		}

		void OnTriggerEnter(Collider collider)
		{
			if (collider.transform.tag == "Player")
			{
				gm.actualGold++;
				gm.AddScore();
				StartCoroutine(getMoney());
			}
		}

		public IEnumerator getMoney()
		{
			//Set the new material instance on the door
			MeshRenderer mr = transform.GetChild(0).GetComponent<MeshRenderer>();

			if (transform.childCount > 0)
			{
				float dissolve = 4.25f;
				float elapsedTime = 0f;
				float waitTime = 2f;

				SoundManager.current.piecePicked();

				while (elapsedTime < waitTime)
				{

					dissolve = Mathf.Lerp(dissolve, 0, (elapsedTime / waitTime));
					elapsedTime += Time.unscaledDeltaTime;

					mr.materials[0].SetFloat("_CutoffHeight", dissolve);
					// Yield here
					yield return null;
				}

				// Make sure we got there
				mr.materials[0].SetFloat("_CutoffHeight", 0f);
			}

			if (!isLinkedToRoom)
			{
				manageRespawn();
			}
			else
			{
				gameObject.SetActive(false);
			}

			yield return null;
		}

		public void manageRespawn()
		{
			int p = Random.Range(0, gm.allTiles.Length);
			transform.position = gm.LocationOkToSpawn(new Vector3(gm.allTiles[p].transform.position.x, 1, gm.allTiles[p].transform.position.z));
			manageVisibility();
		}
	}
}
