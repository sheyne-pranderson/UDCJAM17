﻿using System.Collections;
using UnityEngine;
using UnityEngine.Rendering;


namespace roguelikesnake
{
	public class Enemy : GlobalEntity
	{
		private GameManager gm;
		private Transform renderer;
		public GameObject glowingCrown;
		public GameObject glowingSword;

		// Start is called before the first frame update
		private void Start()
		{
			gm = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
		}

		private void Awake()
		{
			renderer = transform.GetChild(0);
			//StartCoroutine(animateAppearing());
			setInvisible();
		}
		public void setInvisible()
		{
			renderer.gameObject.SetActive(false);
			isVisible = false;
		}

		public void setVisible()
		{
			renderer.gameObject.SetActive(true);
			isVisible = true;
		}

		private void OnTriggerEnter(Collider collider)
		{
			if (collider.transform.tag == "Player")
			{
				if ((gm.actualHp - 1) > 0)
				{
					gm.KillEnemy();
				}
				else
				{
					gm.actualHp = 0;
					//gm.cc.isPause = true;
					Time.timeScale = 0;
					Debug.Log("HE DEAD !");
				}
				StartCoroutine(fightTheEnnemy());
			}
		}


		public IEnumerator fightTheEnnemy()
		{
			//Set the new material instance on the door
			MeshRenderer mr = transform.GetChild(0).GetComponent<MeshRenderer>();

			float originalTimeScale = Time.timeScale;
			Time.timeScale = originalTimeScale / 4;

			UnityEngine.Rendering.Universal.ColorAdjustments colorAdjustments;
			VolumeProfile Volume = GameObject.FindGameObjectWithTag("Volume").GetComponent<Volume>().profile;
			if (!Volume.TryGet(out colorAdjustments)) throw new System.NullReferenceException(nameof(colorAdjustments));
			//ColorParameter colorBase = colorAdjustments.colorFilter;


			if (transform.childCount > 0)
			{
				float dissolve = 4.25f;
				float elapsedTime = 0f;
				float waitTime = 3f;
				float timeBeforeSpeedBack = 0.3f;
				bool isTimeBack = false;

				float timeBeforeColorChange = 0f;
				float colorWaitingTime = 0.4f;
				bool isColorChanged = false;

				SoundManager.current.skelettonKilled();

				while (elapsedTime < waitTime)
				{
					if (!isTimeBack && elapsedTime > timeBeforeSpeedBack)
					{
						isTimeBack = true;
						CameraShake cameraShake = GameObject.FindGameObjectWithTag("MainCamera").transform.GetChild(0).GetComponent<CameraShake>();
						cameraShake.shakeDuration = 0.15f;
						cameraShake.shakeAmount = 0.9f;
						Time.timeScale = originalTimeScale;
						glowingCrown.SetActive(false);
						glowingSword.SetActive(false);
					}

					if(elapsedTime >= timeBeforeColorChange && !isColorChanged)
                    {
						isColorChanged = true;
						colorAdjustments.active = true;
					}else if(isColorChanged && elapsedTime >= timeBeforeColorChange + colorWaitingTime)
                    {
						colorAdjustments.active = false;
					}


					dissolve = Mathf.Lerp(dissolve, 0, (elapsedTime / waitTime));
					elapsedTime += Time.unscaledDeltaTime;

					setAllMaterialsValue("_CutoffHeight", dissolve, renderer);

					// Yield here
					yield return null;
				}
				Time.timeScale = originalTimeScale;
				// Make sure we got there
				setAllMaterialsValue("_CutoffHeight", 0f, renderer);

				yield return null;

				if (!isLinkedToRoom)
				{
					int e = Random.Range(0, gm.allTiles.Length);
					transform.position = gm.LocationOkToSpawn(new Vector3(gm.allTiles[e].transform.position.x, 1, gm.allTiles[e].transform.position.z));
					manageVisibility();
					setAllMaterialsValue("_CutoffHeight", 4.25f, renderer);
					glowingCrown.SetActive(true);
					glowingSword.SetActive(true);
				}
				else
                {
					gameObject.SetActive(false);
				}
			}
			yield return null;
		}


		private void Update()
        {
            if(gm.cc)
            {
				renderer.LookAt(new Vector3(gm.cc.transform.position.x, transform.position.y - 0.5f, gm.cc.transform.position.z));

			}
        }
    }
}