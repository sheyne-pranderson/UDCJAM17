﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace roguelikesnake
{
	public class GlobalEntity : MonoBehaviour
	{
		public bool isVisible = false;
		public bool isLinkedToRoom = false;
		public List<MeshRenderer> meshRenderers = new List<MeshRenderer>();

		public void setVisible()
		{
			if (transform.GetChild(0).GetComponent<MeshRenderer>())
			{
				MeshRenderer mr = transform.GetChild(0).GetComponent<MeshRenderer>();
				mr.enabled = true;
			}
			else
			{
				transform.GetChild(0).gameObject.SetActive(true);
			}
			isVisible = true;
		}

		public void setInvisible()
		{
			if(transform.GetChild(0).GetComponent<MeshRenderer>())
            {
				MeshRenderer mr = transform.GetChild(0).GetComponent<MeshRenderer>();
				mr.enabled = false;
			}
			else
            {
				transform.GetChild(0).gameObject.SetActive(false);
			}
			isVisible = false;
		}

		public IEnumerator animateAppearing()
		{
			if (transform.childCount > 0)
			{
				Transform child = transform.GetChild(0);
				Vector3 baseChildLocalscale = child.localScale;
				child.localScale = new Vector3(0, 0, 0);
				//Animate the tile reapearition
				float scaleX = 0f;
				float scaleZ = 0f;
				float scaleY = 0f;
				float elapsedTime = 0f;
				float waitTime = 4f; //TODO MAKE THIS A GAMEMANAGER VALUE

				while (elapsedTime < waitTime)
				{
					scaleX = Mathf.Lerp(child.localScale.x, baseChildLocalscale.x, (elapsedTime / waitTime));
					scaleZ = Mathf.Lerp(child.localScale.z, baseChildLocalscale.z, (elapsedTime / waitTime));
					scaleY = Mathf.Lerp(child.localScale.y, baseChildLocalscale.y, (elapsedTime / waitTime / 2));
					elapsedTime += Time.deltaTime;

					child.localScale = new Vector3(scaleX, scaleY, scaleZ);
					// Yield here
					yield return null;
				}

				// Make sure we got there
				child.localScale = baseChildLocalscale;
			}
			yield return null;
		}

		public Tile getBottomTile()
        {
			RaycastHit hit;
			if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, Mathf.Infinity))
			{
				if ((hit.collider.CompareTag("FloorTile") || hit.collider.CompareTag("connexion")))
				{
					return hit.collider.GetComponent<Tile>();
				}
			}

			return default(Tile);
		}

		public void manageVisibility()
        {
			Tile tile = getBottomTile();
			if (tile != default(Tile))
			{
				if (!tile.isVisible && isVisible)
				{
					setInvisible();
				}
				else if (tile.isVisible && !isVisible)
				{
					setVisible();
				}
			}
		}

		public void setAllMaterialsValue(string shaderName, float value, Transform parent)
		{
			if (meshRenderers.Count == 0)
			{
				addAllChildMeshRenderers(parent);
			}


			foreach(MeshRenderer mr in meshRenderers)
            {
				for (int i = 0; i < mr.materials.Length; i++)
                {
					if(mr.materials[i].HasProperty(shaderName))
						mr.materials[i].SetFloat(shaderName, value);
				}
            }
		}

		public void addAllChildMeshRenderers(Transform parent)
		{
			foreach (Transform child in parent)
			{
				if (child.GetComponent<MeshRenderer>())
				{
					meshRenderers.Add(child.GetComponent<MeshRenderer>());
				}
				addAllChildMeshRenderers(child);
			}
		}
	}
}
