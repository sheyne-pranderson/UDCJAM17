﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace roguelikesnake
{
    public class UiFade : MonoBehaviour
    {

        public bool isFadeIn = false;
        public bool startToFade = false;
        public float fadeSpeed = 0.01f;
        public RawImage RI;
        private float alpha;
        public bool startOnAwake = false;

        private void Awake()
        {
            if(isFadeIn)
            {
                RI.color = new Color(RI.color.r, RI.color.g, RI.color.b, 0);
            }
            else
            {
                RI.color = new Color(RI.color.r, RI.color.g, RI.color.b, 1);
            }
            if (startOnAwake)
            {
                StartCoroutine(startAfterSecond());
            }
        }

        public IEnumerator startAfterSecond()
        {
            yield return new WaitForSeconds(1f);
            startToFade = true;
        }

        // Update is called once per frame
        void Update()
        {
            if(startToFade)
            {
                alpha = RI.color.a;
                if (isFadeIn)
                {
                    alpha += fadeSpeed;
                    if (alpha > 1)
                    {
                        alpha = 1;
                    }
                }
                else
                {
                    alpha -= fadeSpeed;
                    if(alpha < 0)
                    {
                        alpha = 0;
                    }
                }
                RI.color = new Color(RI.color.r, RI.color.g, RI.color.b, alpha);
            }
        }
    }
}
